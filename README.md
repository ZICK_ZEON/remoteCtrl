# remoteCtrl

#### 项目介绍
拿hessian封装做的远程调用webservice工具;

目标是实现多种协议实现客户机对主机资源的调用;

现在完成的功能是以客户机远程调用主机的浏览器工具。

#### 软件架构
IMWS:部署于主机端，接受IMCLIENT发来的指令操作主机资源。
IMCLIENT:放在客户端，用于向IMWS发布指令。
IMSERVER:部署于主机端，以web页面方式管理所有主机。


#### 安装教程

1. 主机端部署tomcat容器，将IMWS、IMSERVER部署于上，并打开主机对应端口。
2. 将IMCLIENT打成可执行文件，或者JAR包并调用。


#### 使用说明

1. 主机端部署tomcat容器，将IMWS部署于上，并打开主机对应端口。
2. 将IMCLIENT打成可执行文件，或者JAR包并调用。