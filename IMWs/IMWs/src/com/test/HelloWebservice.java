package com.test;

import javax.jws.WebService;


/** 
* Java6开发的WebService服务端 
* 
* @author leizhimin 2009-11-16 10:24:13 
*/ 
@WebService 
public class HelloWebservice {
    /** 
     * Web服务中的业务方法 
     * 
     * @return 一个字符串 
     */ 
    public String doSomething(String username) { 
            return username + " is doing something!"; 
            
    } 
	
//	public void doGet(HttpServletRequest request, HttpServletResponse response)
//			throws ServletException, IOException {
//		response.sendRedirect("http://172.20.6.197:8080/x5/UI2/v_/chat/fromChat.w?device=m&chat="+ URLEncoder.encode("你好", "UTF-8"));
//	}
	
}