package com.hessian.token;

/**
 * @author zhangwei
 *
 */
public interface EncryptionService {
	
	public static final String publicKeyFile = EncryptionService.class.getResource("/").getPath() + "/public_key_file.rsa";
	public static final String privateKeyFile = EncryptionService.class.getResource("/").getPath() + "/private_key_file.rsa";
	public static final String aesKeyFile = EncryptionService.class.getResource("/").getPath() + "AES_Secret_Key_File.aes";
	
	public byte[] testHessianEncrypt(byte[] encryptedBytes);

}
