/**
 * 
 */
package com.hessian.token.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;

/**
 * @author zhangwei
 *
 */
public class KeyUtil {
	
	public static Object getKey(String keyFile) throws Exception{
		File file = new File(keyFile);
		FileInputStream in = new FileInputStream(file);
		ObjectInputStream objectInput = new ObjectInputStream(in);
		Object obj = objectInput.readObject();
		objectInput.close();
		return obj;
	}

}
