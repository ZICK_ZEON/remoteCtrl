package com.hessian.token.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Properties;

public class ProUtil {
   
    private ProUtil (){};   
    public static Properties  propertie = null;
    static {
        propertie = new Properties();
        InputStream inputStream = ProUtil.class.getResourceAsStream("/config.properties");
        BufferedReader buff = new BufferedReader(new InputStreamReader(inputStream));

    try {
            propertie.load(buff);
            inputStream.close();
        } catch (IOException e) {
        }
    }
    
    public static void main(String[] args) {
        System.out.println("--->"+ProUtil.propertie.get("name"));  //name
    }
}