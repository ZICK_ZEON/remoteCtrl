/**
 * 
 */
package com.hessian.token.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * @author zhangwei
 * 
 */
public class SerializationUtil {

	public static byte[] serialize(Object obj) {
		try {
			if (obj == null)
				throw new NullPointerException();
			
			ByteArrayOutputStream os = new ByteArrayOutputStream();
			ObjectOutputStream out = new ObjectOutputStream(os);
			out.writeObject(obj);
			out.flush();
			out.close();
			return os.toByteArray();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static Object deserialize(byte[] by) {
		try {
			if (by == null)
				throw new NullPointerException();

			ByteArrayInputStream is = new ByteArrayInputStream(by);
			ObjectInputStream in = new ObjectInputStream(is);
			Object obj = in.readObject();
			in.close();
			return obj;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
