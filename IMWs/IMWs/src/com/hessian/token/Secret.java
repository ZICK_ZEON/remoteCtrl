/**
 * 
 */
package com.hessian.token;

/**
 * @author zhangwei
 *
 */
public class Secret implements java.io.Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8318695868355712468L;
	private String message;

	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
}
