package com.hessian.token.impl;

import com.hessian.token.EncryptionService;
import com.hessian.token.Secret;
import com.hessian.token.util.AESEncryptionUtil;
import com.hessian.token.util.SerializationUtil;

/**
 * @author zhangwei
 *
 */
public class EncryptionServiceImpl implements EncryptionService{

	public byte[] testHessianEncrypt(byte[] encryptedBytes) {
		//解密参数
		byte[] decryptedBytes = AESEncryptionUtil.decryptMessage(encryptedBytes, aesKeyFile);
		//将参数反序列化
		Secret secret = (Secret)SerializationUtil.deserialize(decryptedBytes);
		//业务逻辑
		System.out.println(secret.getMessage());
		secret.setMessage("I love you too!");
		System.out.println(secret.getMessage());
		//将结果序列化
		byte[] returnBytes = SerializationUtil.serialize(secret);
		//加密返回结果
		byte[] encryptedReturnBytes = AESEncryptionUtil.encryptMessage(returnBytes,aesKeyFile);
		return encryptedReturnBytes;
	}
}
