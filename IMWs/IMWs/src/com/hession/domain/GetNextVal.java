package com.hession.domain;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.naming.NamingException;


public class GetNextVal {
	public static int genSequenceNo(String snCode,int count,java.sql.Connection conn) throws NamingException{
		 int ID = 0;
		  //加载驱动
		try{
		   //DriverManager.registerDriver(new sun.jdbc.odbc.JdbcOdbcDriver());
		   //获得连接
			
		        java.sql.PreparedStatement pstmt = null;
		       
		        String f_getnextval = "select f_getnextval('"+snCode+"',"+count+");";
				pstmt = conn.prepareStatement(f_getnextval);
				ResultSet rs = pstmt.executeQuery();
				if(rs.next()) {
					ID = rs.getInt(1);
				}  
		}catch(SQLException e1){
			e1.printStackTrace();
		}
		return ID;
		
	}
}
