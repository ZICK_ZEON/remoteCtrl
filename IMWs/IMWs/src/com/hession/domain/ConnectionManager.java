package com.hession.domain;

import java.sql.Connection;
import java.sql.SQLException;

import com.mchange.v2.c3p0.ComboPooledDataSource;

public class ConnectionManager {
	public static ComboPooledDataSource dataSource;  
	static {  
	    try {  
	        dataSource = new ComboPooledDataSource();  
	        dataSource.setUser("root");  
	        dataSource.setPassword("123456");  
	        dataSource.setJdbcUrl("jdbc:mysql://localhost:3306/shiro");  
	        //dataSource.setJdbcUrl("jdbc:mysql://172.20.88.13:3306/shiro");  
	        dataSource.setDriverClass("com.mysql.jdbc.Driver");  
	        dataSource.setInitialPoolSize(10);  
	        dataSource.setMinPoolSize(5);  
	        dataSource.setMaxPoolSize(50);  
	        dataSource.setMaxStatements(100);  
	        dataSource.setMaxIdleTime(60);  
	    } catch (Exception e) {  
	        e.printStackTrace();  
	    }  
	}  
	/** 
	 * 从连接池中获取数据源链接 
	 *  
	 * @author gaoxianglong 
	 *  
	 * @return Connection 数据源链接 
	 */  
	public static Connection getConnection() {  
	    Connection conn = null;  
	    if (null != dataSource) {  
	        try {  
	            conn = dataSource.getConnection();  
	        } catch (SQLException e) {  
	            e.printStackTrace();  
	        }  
	    }  
	    return conn;  
	}  
	
}
