package com.hession.service;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hessian.token.EncryptionService;
import com.hession.back.AccountResultbak;
import com.hession.back.AppResult;
import com.hession.back.CommonResult;
import com.hession.back.DeptResult;
import com.hession.back.LoginResult;
import com.hession.back.OrgResult;
import com.hession.back.RegisterResult;
import com.hession.back.ResourceResult;
import com.hession.back.RoleResult;
import com.hession.domain.Account;
import com.hession.domain.LoginUser;


public interface DFRightService {
	
	//安全签名加密参数  
    public void setSinagure(String sinatrue);  
    public void setNonce(String nonce);  
    public void setTimestamp(String timestamp);  
	
	/**
	 * 注册组织机构，生成权限系统主键orgId
	 * @param orgCode
	 * @param orgName
	 * @return
	 */
	public RegisterResult registerOrg(String orgCode,String orgName);
	
	/**
	 * 注册账号
	 * @param orgId
	 * @param account
	 * @param accountName
	 * @param pwd
	 * @return
	 */
	public RegisterResult register(String orgId,String account,String accountName,String pwd);
	
	public LoginResult login(LoginUser loginUser);
	/**
	 * 获取权限
	 * @return
	 */
	public ResourceResult getRights(LoginUser loginUser);//获取某个账号的所有权限
	public CommonResult queryIsUserRes(LoginUser loginUser,List<String> resCode,String accountCode);//查询某个账号是否有某些权限
	public CommonResult queryAccountHasRes(LoginUser loginUser,String accountCode,String resCode);//查询某个账号是否有某个角色
	public CommonResult createUsers(LoginUser loginUser,List<Account> accs);//注册账号
	public CommonResult queryAccountState(LoginUser loginUser,String accountCode);//查询某个账号是否被禁用
	public OrgResult queryUserOrg(LoginUser loginUser,String accountCode);//查询某个账号所属的组织
	public DeptResult queryUserDept(LoginUser loginUser,String accountCode);//查询某个账号所属的部门
	public AccountResultbak queryAccountDetail(LoginUser loginUser,String accountCode);//查询某个账号信息
	public CommonResult queryResState(LoginUser loginUser,String resCode);//查询某个资源是否被禁用
	public ResourceResult queryResDetail(LoginUser loginUser,String resCode,String resName);//查询某个资源的详细信息
	public CommonResult resIsBelongOrg(LoginUser loginUser,String resCode);//查询某个资源是否属于某个组织
	public CommonResult resOwnRelaton(LoginUser loginUser,String resCode1,String resCode2);//查询两个资源是否需要关联申请
	public OrgResult queryResOrg(LoginUser loginUser,String resCode);//查询资源所属组织
	public AppResult queryResApp(LoginUser loginUser,String resCode);//查询资源所属应用
	public RoleResult queryResRole(LoginUser loginUser,String resCode);//查询资源所属的角色
	public AccountResultbak queryResAccount(LoginUser loginUser,String resCode);//查询资源所属用户
	public DeptResult queryDeptDetail(LoginUser loginUser,String deptName);//查询部门信息
	public DeptResult queryUpDept(LoginUser loginUser,String deptId);//查询部门的上级部门信息
	public DeptResult queryNextDept(LoginUser loginUser,String deptId);//查询部门的下级部门信息
	public AccountResultbak queryDeptAccount(LoginUser loginUser,String deptId);//查询某个部门的人员
	public CommonResult queryDeptState(LoginUser loginUser,String deptId);//查询某个部门是否被禁用
	public OrgResult queryOrgDetail(LoginUser loginUser);//查询组织详细信息
	public AppResult queryOrgApps(LoginUser loginUsere);//查询组织的所有应用信息
	public ResourceResult queryOrgResources(LoginUser loginUser);//查询组织所有资源信息
	public DeptResult queryOrgDepts(LoginUser loginUser);//查询组织的部门信息
	public AccountResultbak queryOrgAccount(LoginUser loginUser);//查询组织的人员信息
	public CommonResult queryOrgState(LoginUser loginUser);//查询组织是否禁用
	public AppResult queryAppDetail(LoginUser loginUser);//查询应用详情
	public OrgResult queryAppOrg(LoginUser loginUser);//查询应用所属的组织
	public ResourceResult queryAppRes(LoginUser loginUser);//查询应用资源
	public RoleResult queryAppRoles(LoginUser loginUser);//查询应用对应的角色
	public CommonResult queryAppState(LoginUser loginUser);//查询应用状态
	public LoginResult logout(String orgId,String account );//退出登录
	
	public boolean doGet(String accountCode,String Message) throws ServletException, IOException;
	
	public boolean doGroupGet(List<String> accountCodes,String Message)	throws ServletException, IOException;
	
	public boolean doGroupArrGet(String[] accountCodesArr,String Message)	throws ServletException, IOException;
}
