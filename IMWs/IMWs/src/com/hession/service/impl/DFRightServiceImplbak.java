package com.hession.service.impl;

import java.io.IOException;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.mgt.DefaultSecurityManager;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.util.JdbcUtils;

import com.hessian.token.TokenUtil;
import com.hessian.token.util.ProUtil;
import com.hession.back.AccountResultbak;
import com.hession.back.AppResult;
import com.hession.back.CommonResult;
import com.hession.back.DeptResult;
import com.hession.back.LoginResult;
import com.hession.back.OrgResult;
import com.hession.back.RegisterResult;
import com.hession.back.ResourceResult;
import com.hession.back.RoleResult;
import com.hession.domain.AIOrg;
import com.hession.domain.AIUser;
import com.hession.domain.Account;
import com.hession.domain.App;
import com.hession.domain.ConnectionManager;
import com.hession.domain.DFJdbcRealm;
import com.hession.domain.Dept;
import com.hession.domain.GetNextVal;
import com.hession.domain.LoginUser;
import com.hession.domain.Org;
import com.hession.domain.Resource;
import com.hession.domain.Role;
import com.hession.domain.User;
import com.hession.service.DFRightService;
import com.test.Myservlet;

public class DFRightServiceImplbak  implements DFRightService  {

	static DFJdbcRealm dfRealm;
	static {
		dfRealm = new DFJdbcRealm();

		dfRealm.setDataSource(ConnectionManager.dataSource);

		dfRealm.setPermissionsLookupEnabled(true);

		dfRealm.setAuthenticationQuery("SELECT password FROM ai_account WHERE accountId = ?");

		dfRealm.setUserRolesQuery("SELECT roleId FROM ai_account_role WHERE accountId = (SELECT accountId FROM ai_account WHERE accountId = ?)");

		dfRealm.setPermissionsQuery("SELECT resId FROM ai_role_res WHERE roleId in (SELECT roleId FROM ai_roles WHERE roleId = ?)");

		DefaultSecurityManager security = new DefaultSecurityManager(dfRealm);

		SecurityUtils.setSecurityManager(security);
	}

	private String signature;
	private String nonce;
	private String timestamp;

	public void setSinagure(String sinature) {
		this.signature = sinature;
	}

	public void setNonce(String nonce) {
		this.nonce = nonce;

	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	/** 注册组织 */
	public RegisterResult registerOrg(String orgCode, String orgName) {
		if (TokenUtil.validateSignature(signature, timestamp, nonce)) {
			RegisterResult result = new RegisterResult();
			try {
				if (isHaveOrg(orgCode, orgName)) {
					result.setCode("1");
					result.setMessage("注册失败,存在该公司");
					return result;
				}
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			Connection conn = ConnectionManager.getConnection();
			String authenticationQuery = "insert into ai_org (orgId,orgCode,orgName) values (?,?,?)";
			PreparedStatement ps = null;
			ResultSet rs = null;

			try {
				ps = conn.prepareStatement(authenticationQuery);
				// ps.setString(1, orgId);
				// ps.setString(2, orgCode);
				// ps.setString(3, orgName);
				ps.execute();

				// result.setId(orgId) ;

			} catch (Exception e) {
				result.setCode("1");
				result.setMessage("注册失败");
			} finally {
				JdbcUtils.closeResultSet(rs);
				JdbcUtils.closeStatement(ps);
			}

			return result;
		} else {
			return null;
		}

	}

	/** 登录 */
	public LoginResult login(LoginUser loginUser) {

		if (TokenUtil.validateSignature(signature, timestamp, nonce)) {

			LoginResult lresult = new LoginResult();
			AIUser rUser = null;
			String orgId = "";
			String domain = loginUser.getDomain();
			String account = loginUser.getAccountCode();
			String pwd = loginUser.getPwd();
			try {
				orgId = queryOrgByDomain(domain);
				rUser = queryPwdByAccount(orgId, account);
				if (rUser == null || rUser.getPassword() == null
						|| rUser.getPassword() == null
						|| !pwd.equals(rUser.getPassword())) {
					lresult.setCode("1");
					lresult.setMessage("账号密码不能为空");
					return lresult;
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}

			Subject currentUser = SecurityUtils.getSubject();
			UsernamePasswordToken token = new UsernamePasswordToken(
					rUser.getAccountId(), pwd);
			if (!currentUser.isAuthenticated()) {

				token.setRememberMe(true);
				try {
					currentUser.login(token);
					System.out.println("currentUser.getSession()="
							+ currentUser);
					System.out.println("login successfully");
					System.out.println("User [" + currentUser.getPrincipal()
							+ "] logged in successfully.");
					lresult.setCode("0");
					lresult.setMessage("登录成功");
				} catch (UnknownAccountException uae) {
					System.out.println("There is no user with username of "
							+ token.getPrincipal());
					lresult.setCode("1");
					lresult.setMessage("没有该用户名");
				} catch (IncorrectCredentialsException ice) {
					System.out.println("Password for account "
							+ token.getPrincipal() + " was incorrect!");
					lresult.setCode("1");
					lresult.setMessage("账号密码不对");
				} catch (LockedAccountException lae) {
					System.out
							.println("The account for username "
									+ token.getPrincipal()
									+ " is locked.  "
									+ "Please contact your administrator to unlock it.");
					lresult.setCode("1");
					lresult.setMessage("账号锁定");
				} catch (AuthenticationException ae) {
					lresult.setCode("1");
					lresult.setMessage("认证失败");
				}
			} else {
				lresult.setCode("0");
				lresult.setMessage("已经登录");
			}
			return lresult;
		} else {
			return null;
		}

	}

	public LoginResult logout(String domain, String account) {
		String orgId = "";
		LoginResult lresult = new LoginResult();
		try {
			orgId = queryOrgByDomain(domain);
			if (isHaveAccount(orgId, account) == null
					|| "".equals(isHaveAccount(orgId, account))) {
				lresult.setCode("1");
				lresult.setMessage("注销失败");
				return lresult;
			}
			Subject currentUser = SecurityUtils.getSubject();
			if (currentUser.isAuthenticated()) {
				currentUser.logout();
				lresult.setCode("0");
				lresult.setMessage("注销成功");
			} else {
				lresult.setCode("1");
				lresult.setMessage("没有登录，无法注销");
			}
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		return lresult;
	}

	/**
	 * 查询某个账号某个应用的资源
	 * 
	 * @param loginuser
	 * 
	 */
	@Override
	public ResourceResult getRights(LoginUser loginUser) {

		if (TokenUtil.validateSignature(signature, timestamp, nonce)) {
			ResourceResult rResult = new ResourceResult();
			LoginResult lResult = login(loginUser);// 登录
			LoginUser lUser = getLoginUser(loginUser);
			List<Resource> res = new ArrayList<Resource>();
			String domain = lUser.getDomain();
			String orgId = "";
			String account = lUser.getAccountCode();
			String appid = lUser.getAppid();
			if (lResult.getCode() != null && !"".equals(lResult.getCode())
					&& lResult.getCode().equals("0")) {
				Connection conn = ConnectionManager.getConnection();
				String authenticationQuery = "SELECT t4.* FROM ai_account t1,ai_account_role t2,ai_role_res t3,ai_resource t4 WHERE t1.accountId=t2.accountId AND t2.roleId=t3.roleId AND t3.resId=t4.resId and t4.state='1' and t1.orgId=? and t4.appId=? and t1.accountCode=?";
				PreparedStatement ps = null;
				ResultSet rs = null;
				try {
					orgId = queryOrgByDomain(domain);
					ps = conn.prepareStatement(authenticationQuery);
					ps.setString(1, orgId);
					ps.setString(2, appid);
					ps.setString(3, account);
					rs = ps.executeQuery();
					while (rs.next()) {
						Resource resource = new Resource();
						resource.setId(rs.getInt("id"));
						resource.setResContent(rs.getString("resContent"));
						resource.setResCode(rs.getString("resCode"));
						resource.setResDes(rs.getString("resDes"));
						resource.setResId(rs.getString("resId"));
						resource.setResName(rs.getString("resName"));
						resource.setCreateDate(rs.getDate("createDate"));
						resource.setState(rs.getString("state"));
						resource.setType(rs.getString("type"));
						resource.setAppId(rs.getString("appId"));
						res.add(resource);
					}
					if (res.size() > 0) {
						rResult.setCode(0);
						rResult.setMsg("资源获取成功！");
						rResult.setResources(res);
					}
				} catch (Exception e) {
				} finally {
					JdbcUtils.closeResultSet(rs);
					JdbcUtils.closeStatement(ps);
				}
				Subject currentUser = SecurityUtils.getSubject();
				// LoginResult logout =
				// logout(loginUser.getDomain(),loginUser.getAccountCode());
				currentUser.logout();
			}

			return rResult;
		} else {
			return null;
		}

	}

	public CommonResult queryIsUserRes(LoginUser loginUser, List<String> res,
			String accountCode) {

		if (TokenUtil.validateSignature(signature, timestamp, nonce)) {
			CommonResult cResult = new CommonResult();
			LoginResult lResult = login(loginUser);// 登录

			// 处理传来的参数，获取用户信息Start
			LoginUser lUser = getLoginUser(loginUser);
			String domain = lUser.getDomain();
			String orgId = "";
			String appid = lUser.getAppid();
			// end
			String noResource = "";
			if (lResult.getCode() != null && !"".equals(lResult.getCode())
					&& lResult.getCode().equals("0")) {
				PreparedStatement ps = null;
				ResultSet rs = null;

				try {
					ResourceResult resourceResult = getRights(loginUser);// 根据传来的参数loginUser
																			// 查询拥有的权限
					List<Resource> isResources = resourceResult.getResources();// 所有的资源列表
					List<String> isResList = new ArrayList<String>();
					for (int i = 0; i < isResources.size(); i++) {
						Resource resource = isResources.get(i);
						String resCode = resource.getResCode();
						isResList.add(resCode);
					}
					for (int i = 0; i < res.size(); i++) {
						String tmpResId = res.get(i);
						if (!isResList.contains(tmpResId)) {
							noResource += tmpResId + ",";
						}
					}
					if (noResource.length() > 0) {
						cResult.setCode(1);
						cResult.setMsg("该用户对以下资源没有权限：" + noResource
								+ "请联系管理员进行授权！");
					} else {
						cResult.setCode(1);
						cResult.setMsg("该用户有本次查询的所有资源！");
					}
				} catch (Exception e) {
				} finally {
					JdbcUtils.closeResultSet(rs);
					JdbcUtils.closeStatement(ps);
				}
			}
			Subject currentUser = SecurityUtils.getSubject();
			// LoginResult logout =
			// logout(loginUser.getDomain(),loginUser.getAccountCode());
			currentUser.logout();
			return cResult;
		} else {
			return null;
		}

	}

	/**
	 * 注册账号
	 * 
	 * @param orgCode
	 * @param account
	 * @param accountName
	 * @param pwd
	 * @return
	 * @throws SQLException
	 */
	public RegisterResult register(String orgCode, String account,
			String accountName, String pwd) {

		if (TokenUtil.validateSignature(signature, timestamp, nonce)) {
			RegisterResult result = new RegisterResult();
			try {
				if (StringUtils.isEmpty(isHaveAccount(orgCode, account))) {
					result.setCode("1");
					result.setMessage("注册失败");
					return result;
				}
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			Connection conn = ConnectionManager.getConnection();
			String authenticationQuery = "insert into ai_account (accountName,accountCode,password) values (?,?,?)";
			PreparedStatement ps = null;
			ResultSet rs = null;

			try {
				ps = conn.prepareStatement(authenticationQuery);
				ps.setString(1, accountName);
				ps.setString(2, account);
				ps.setString(3, pwd);
				ps.execute();

				String sql = "select accountId from ai_account WHERE accountCode =?";
				ps = conn.prepareStatement(sql);
				ps.setString(1, account);
				rs = ps.executeQuery();
				while (rs.next()) {
					result.setId(rs.getLong("accountId"));
				}

			} catch (Exception e) {
				result.setCode("1");
				result.setMessage("注册失败");
			} finally {
				JdbcUtils.closeResultSet(rs);
				JdbcUtils.closeStatement(ps);
			}

			return result;
		} else {
			return null;
		}

	}

	/**
	 * 注册账号 createUsers
	 * 
	 * @param List
	 *            <Account>
	 * @param domain
	 * 
	 * 
	 */
	public CommonResult createUsers(LoginUser loginUser, List<Account> accs) {

		if (TokenUtil.validateSignature(signature, timestamp, nonce)) {
			CommonResult cResult = new CommonResult();
			String orgId = "";
			LoginResult lResult = login(loginUser);// 登录
			LoginUser lUser = getLoginUser(loginUser);
			String domain = lUser.getDomain();
			if (lResult.getCode() != null && !"".equals(lResult.getCode())
					&& lResult.getCode().equals("0")) {
				try {
					orgId = queryOrgByDomain(domain);
					String fmsg = "";

					for (Account acc : accs) {
						boolean flag = StringUtils.isEmpty(isHaveAccount(orgId,
								acc.getAccountCode()));
						if (!flag) {// 如果flag为true，代表查询没有该账户
							fmsg += acc.getAccountCode() + ",";
						}
					}
					if (fmsg.length() > 0) {
						cResult.setCode(1);
						cResult.setMsg(fmsg + "已注册，注册失败！");// 账号已存在，注册失败
					} else {
						Connection conn = ConnectionManager.getConnection();
						PreparedStatement ps = null;
						ResultSet rs = null;
						String authenticationQuery = "insert into ai_account (id,accountId,accountName,accountCode,password,orgId,state,createDate) values (?,?,?,?,?,?,?,?)";
						int id = GetNextVal.genSequenceNo("accountId",
								accs.size(), conn);// 获取ID
						Date cDate = new java.sql.Date(
								System.currentTimeMillis());// 系统当前时间
						for (int i = 0; i < accs.size(); i++) {
							Account account = accs.get(i);
							ps = conn.prepareStatement(authenticationQuery);
							ps.setInt(1, id - i);
							ps.setString(2, "P" + String.valueOf(id - i));
							ps.setString(3, account.getAccountName());
							ps.setString(4, account.getAccountCode());
							ps.setString(5, account.getPassword());
							ps.setString(6, orgId);
							ps.setString(7, "1");
							ps.setDate(8, cDate);
							ps.execute();
						}
						cResult.setCode(0);
						cResult.setMsg("注册成功！");// 账号已存在，注册失败
					}
				} catch (SQLException e1) {
					e1.printStackTrace();
				} catch (NamingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				Subject currentUser = SecurityUtils.getSubject();
				// LoginResult logout =
				// logout(loginUser.getDomain(),loginUser.getAccountCode());
				currentUser.logout();
			}
			return cResult;
		} else {
			return null;
		}

	}

	/**
	 * 查询某个账号所属的组织
	 * 
	 * @param loginUser
	 * @param account
	 * */
	public OrgResult queryUserOrg(LoginUser loginUser, String account) {

		if (TokenUtil.validateSignature(signature, timestamp, nonce)) {

			// TODO Auto-generated method stub
			OrgResult oResult = new OrgResult();
			LoginResult lResult = login(loginUser);// 登录
			LoginUser lUser = getLoginUser(loginUser);
			String domain = lUser.getDomain();
			if (lResult.getCode() != null && !"".equals(lResult.getCode())
					&& lResult.getCode().equals("0")) {
				Connection conn = ConnectionManager.getConnection();
				PreparedStatement ps = null;
				ResultSet rs = null;
				List<Org> orgs = new ArrayList<Org>();
				String sql = "SELECT t2.* FROM ai_account t1,ai_org t2 WHERE t1.orgId=t2.orgId AND t2.orgId=? and t1.accountCode=?";
				try {
					String orgid = queryOrgByDomain(domain);
					ps = conn.prepareStatement(sql);
					ps.setString(1, orgid);
					ps.setString(2, account);
					rs = ps.executeQuery();
					while (rs.next()) {
						Org org = new Org();
						org.setId(rs.getInt("id"));
						org.setOrgCode(rs.getString("orgCode"));
						org.setOrgId(rs.getString("orgId"));
						org.setOrgName(rs.getString("orgName"));
						org.setCreateDate(rs.getDate("createDate"));
						org.setDomain(rs.getString("domain"));
						org.setState(rs.getString("state"));
						orgs.add(org);
					}
					if (orgs.size() > 0) {
						oResult.setCode(0);
						oResult.setMsg("查询成功！");
						oResult.setOrgs(orgs);
					} else {
						oResult.setCode(1);
						oResult.setMsg("没有该用户组织信息！");
					}
				} catch (SQLException e) {
					// TODO: handle exception
				}
				// 退出登录
				Subject currentUser = SecurityUtils.getSubject();
				currentUser.logout();
			}
			return oResult;
		} else {
			return null;
		}
	}

	/**
	 * 查询账号所在部门
	 * 
	 * @param loginUser
	 * @param accountCode
	 * 
	 * */

	public DeptResult queryUserDept(LoginUser loginUser, String accountCode) {

		if (TokenUtil.validateSignature(signature, timestamp, nonce)) {

			DeptResult deptResult = new DeptResult();
			LoginUser lUser = getLoginUser(loginUser);
			String domain = lUser.getDomain();
			LoginResult lResult = login(loginUser);// 登录
			if (lResult.getCode() != null && !"".equals(lResult.getCode())
					&& lResult.getCode().equals("0")) {
				Connection conn = ConnectionManager.getConnection();
				PreparedStatement ps = null;
				ResultSet rs = null;
				List<Dept> depts = new ArrayList<Dept>();
				String sql = "SELECT t2.* FROM ai_account t1,ai_dept t2 WHERE t1.deptId=t2.deptId AND t1.orgId=? and t1.accountCode=?";
				try {
					String orgid = queryOrgByDomain(domain);
					ps = conn.prepareStatement(sql);
					ps.setString(1, orgid);
					ps.setString(2, accountCode);
					rs = ps.executeQuery();
					while (rs.next()) {
						Dept dept = new Dept();
						dept.setId(rs.getInt("id"));
						dept.setCreateDate(rs.getDate("createDate"));
						dept.setDeptId(rs.getString("deptId"));
						dept.setDeptName(rs.getString("deptName"));
						dept.setOrgId(rs.getString("orgId"));
						dept.setState(rs.getString("state"));
						dept.setUpDeptId(rs.getString("upDeptId"));
						depts.add(dept);
					}
					if (depts.size() > 0) {
						deptResult.setCode(0);
						deptResult.setMsg("查询成功！");
						deptResult.setDepts(depts);
					} else {
						deptResult.setCode(1);
						deptResult.setMsg("没有该用户的部门信息！");
					}
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
			return deptResult;
		} else {
			return null;
		}

	}

	/***
	 * 查询某个账号信息
	 * 
	 * @param loginUser
	 * @param accountCode
	 * 
	 */
	public AccountResultbak queryAccountDetail(LoginUser loginUser,
			String accountCode) {

		if (TokenUtil.validateSignature(signature, timestamp, nonce)) {

			AccountResultbak accountResult = new AccountResultbak();
			LoginUser lUser = getLoginUser(loginUser);
			String domain = lUser.getDomain();
			LoginResult lResult = login(loginUser);// 登录
			if (lResult.getCode() != null && !"".equals(lResult.getCode())
					&& lResult.getCode().equals("0")) {
				Connection conn = ConnectionManager.getConnection();
				PreparedStatement ps = null;
				ResultSet rs = null;
				List<User> users = new ArrayList<User>();
				String sql = "SELECT t1.* FROM ai_account t1,ai_org t2 WHERE t1.orgId=t2.orgId AND t1.orgId=? and t1.accountCode=?";
				try {
					String orgid = queryOrgByDomain(domain);
					ps = conn.prepareStatement(sql);
					ps.setString(1, orgid);
					ps.setString(2, accountCode);
					rs = ps.executeQuery();
					while (rs.next()) {
						User user = new User();
						user.setAccountCode(rs.getString("accountCode"));
						user.setAccountName(rs.getString("accountName"));
						users.add(user);
					}
					if (users.size() > 0) {
						accountResult.setCode(0);
						accountResult.setMsg("查询成功！");
						accountResult.setAccUsers(users);

					} else {
						accountResult.setCode(1);
						accountResult.setMsg("没有该用户的信息！");
					}
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
			return accountResult;
		} else {
			return null;
		}
	}

	/**
	 * 查询某个账号是否被禁用
	 * 
	 * @param loginUser
	 * @param accountCode
	 */
	public CommonResult queryAccountState(LoginUser loginUser,
			String accountCode) {

		if (TokenUtil.validateSignature(signature, timestamp, nonce)) {

			CommonResult cResult = new CommonResult();
			LoginResult lResult = login(loginUser);// 登录
			LoginUser lUser = getLoginUser(loginUser);
			String domain = lUser.getDomain();
			String orgId = "";
			if (lResult.getCode() != null && !"".equals(lResult.getCode())
					&& lResult.getCode().equals("0")) {
				Connection conn = ConnectionManager.getConnection();
				PreparedStatement ps = null;
				ResultSet rs = null;
				String sql = "select * from ai_account where accountCode=? and state='1'";
				try {
					orgId = queryOrgByDomain(domain);
					boolean flag = StringUtils.isEmpty(isHaveAccount(orgId,
							accountCode));
					if (flag) {// 如果flag为true，代表查询没有该账户
						cResult.setCode(1);
						cResult.setMsg("该账号不存在！");
					} else {
						ps = conn.prepareStatement(sql);
						ps.setString(1, accountCode);
						rs = ps.executeQuery();
						if (rs.next()) {
							cResult.setCode(0);
							cResult.setMsg("该账号未被禁用！");
						} else {
							cResult.setCode(1);
							cResult.setMsg("该账号已被禁用！");
						}
					}
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return cResult;
		} else {
			return null;
		}

	}

	/**
	 * 查询某个账号是否被禁用
	 * 
	 * @param loginUser
	 * @param accountCode
	 * @param resCode
	 */
	public CommonResult queryAccountHasRes(LoginUser loginUser,
			String accountCode, String resCode) {

		if (TokenUtil.validateSignature(signature, timestamp, nonce)) {

			CommonResult cResult = new CommonResult();
			LoginResult lResult = login(loginUser);// 登录

			String noResource = "";
			if (lResult.getCode() != null && !"".equals(lResult.getCode())
					&& lResult.getCode().equals("0")) {
				PreparedStatement ps = null;
				ResultSet rs = null;

				try {
					ResourceResult resourceResult = getRights(loginUser);// 根据传来的参数loginUser
																			// 查询拥有的权限
					List<Resource> isResources = resourceResult.getResources();// 所有的资源列表
					for (int i = 0; i < isResources.size(); i++) {
						Resource resource = isResources.get(i);
						String resId = resource.getResId();
						if (resCode != resId) {
							noResource = resource.getResName();
						}
					}
					if (noResource.length() > 0) {
						cResult.setCode(1);
						cResult.setMsg("该用户对以下资源没有权限：" + noResource
								+ ",请联系管理员进行授权！");
					} else {
						cResult.setCode(1);
						cResult.setMsg("该用户有本次查询的所有资源！");
					}
				} catch (Exception e) {
				} finally {
					JdbcUtils.closeResultSet(rs);
					JdbcUtils.closeStatement(ps);
				}
			}
			Subject currentUser = SecurityUtils.getSubject();
			// LoginResult logout =
			// logout(loginUser.getDomain(),loginUser.getAccountCode());
			currentUser.logout();
			return cResult;
		} else {
			return null;
		}

	}

	/**
	 * 查询某个资源是否被禁用
	 * 
	 * @param loginUser
	 * @param resId
	 */
	public CommonResult queryResState(LoginUser loginUser, String resCode) {

		if (TokenUtil.validateSignature(signature, timestamp, nonce)) {

			CommonResult cResult = new CommonResult();
			LoginResult lResult = login(loginUser);// 登录

			LoginUser lUser = getLoginUser(loginUser);
			String appid = lUser.getAppid();

			Connection conn = ConnectionManager.getConnection();
			String sql = "SELECT * FROM ai_resource WHERE resCode=? AND appId=?";
			PreparedStatement ps = null;
			ResultSet rs = null;
			if (lResult.getCode() != null && !"".equals(lResult.getCode())
					&& lResult.getCode().equals("0")) {
				try {
					ps = conn.prepareStatement(sql);
					ps.setString(1, resCode);
					ps.setString(2, appid);
					rs = ps.executeQuery();
					if (rs.next()) {
						if (rs.getString("state").equals("1")) {
							cResult.setCode(0);
							cResult.setMsg("该资源已启用！");
						} else {

							cResult.setCode(1);
							cResult.setMsg("该资源已被禁用！");
						}
					} else {
						cResult.setCode(2);
						cResult.setMsg("该资源不存在！");

					}
				} catch (Exception e) {
				} finally {
					JdbcUtils.closeResultSet(rs);
					JdbcUtils.closeStatement(ps);
				}
			}
			Subject currentUser = SecurityUtils.getSubject();
			// LoginResult logout =
			// logout(loginUser.getDomain(),loginUser.getAccountCode());
			currentUser.logout();
			return cResult;
		} else {
			return null;
		}

	}

	public ResourceResult queryResDetail(LoginUser loginUser, String resCode,
			String resName) {

		if (TokenUtil.validateSignature(signature, timestamp, nonce)) {

			ResourceResult rResult = new ResourceResult();
			LoginResult lResult = login(loginUser);// 登录

			LoginUser lUser = getLoginUser(loginUser);
			String appid = lUser.getAppid();
			List<Resource> resources = new ArrayList<Resource>();
			Connection conn = ConnectionManager.getConnection();
			String sql = "SELECT * FROM ai_resource WHERE (resCode=? OR resName like ?) AND appId=?";
			PreparedStatement ps = null;
			ResultSet rs = null;
			if (lResult.getCode() != null && !"".equals(lResult.getCode())
					&& lResult.getCode().equals("0")) {
				try {
					ps = conn.prepareStatement(sql);
					ps.setString(1, resCode);
					ps.setString(2, "%" + resName + "%");
					ps.setString(3, appid);
					rs = ps.executeQuery();
					while (rs.next()) {
						Resource resource = new Resource();
						resource.setId(rs.getInt("id"));
						resource.setCreateDate(rs.getDate("createDate"));
						resource.setResCode(rs.getString("resCode"));
						resource.setResContent(rs.getString("resContent"));
						resource.setResDes(rs.getString("resDes"));
						resource.setResId(rs.getString("resId"));
						resource.setResName(rs.getString("resName"));
						resource.setState(rs.getString("state"));
						resource.setType(rs.getString("type"));
						resource.setAppId(appid);
						resources.add(resource);
					}
					if (resources.size() > 0) {
						rResult.setCode(0);
						rResult.setMsg("资源详细信息已获取！");
						rResult.setResources(resources);
					} else {
						rResult.setCode(1);
						rResult.setMsg("不存在该资源信息！");
					}
				} catch (Exception e) {
				} finally {
					JdbcUtils.closeResultSet(rs);
					JdbcUtils.closeStatement(ps);
				}
			}
			Subject currentUser = SecurityUtils.getSubject();
			// LoginResult logout =
			// logout(loginUser.getDomain(),loginUser.getAccountCode());
			currentUser.logout();
			return rResult;
		} else {
			return null;
		}

	}

	/**
	 * 查询某个资源是否属于组织
	 * 
	 * @param loginUser
	 * @param resId
	 */
	public CommonResult resIsBelongOrg(LoginUser loginUser, String resCode) {

		if (TokenUtil.validateSignature(signature, timestamp, nonce)) {

			CommonResult cResult = new CommonResult();
			LoginResult lResult = login(loginUser);// 登录

			LoginUser lUser = getLoginUser(loginUser);
			String appid = lUser.getAppid();
			String domain = lUser.getDomain();
			Connection conn = ConnectionManager.getConnection();
			String sql = "SELECT * FROM ai_resource t1,ai_org_res t2 WHERE t1.resId=t2.resId AND t1.appId=t2.appId AND  t1.resCode=? AND t2.appId=? AND t2.orgId=?";
			PreparedStatement ps = null;
			ResultSet rs = null;
			if (lResult.getCode() != null && !"".equals(lResult.getCode())
					&& lResult.getCode().equals("0")) {
				try {
					String orgId = queryOrgByDomain(domain);
					ps = conn.prepareStatement(sql);
					ps.setString(1, resCode);
					ps.setString(2, appid);
					ps.setString(3, orgId);
					rs = ps.executeQuery();
					if (rs.next()) {
						cResult.setCode(0);
						cResult.setMsg("组织" + domain + "有该资源权限！");
					} else {
						cResult.setCode(1);
						cResult.setMsg("组织" + domain + "没有该资源权限！");

					}
				} catch (Exception e) {
				} finally {
					JdbcUtils.closeResultSet(rs);
					JdbcUtils.closeStatement(ps);
				}
			}
			Subject currentUser = SecurityUtils.getSubject();
			// LoginResult logout =
			// logout(loginUser.getDomain(),loginUser.getAccountCode());
			currentUser.logout();
			return cResult;
		} else {
			return null;
		}
	}

	/**
	 * 查询两个资源是否需要关联申请
	 * 
	 * @param loginUser
	 * @param resCode1
	 * @param resCode2
	 */

	public CommonResult resOwnRelaton(LoginUser loginUser, String resCode1,
			String resCode2) {

		if (TokenUtil.validateSignature(signature, timestamp, nonce)) {

			CommonResult cResult = new CommonResult();
			LoginResult lResult = login(loginUser);// 登录

			LoginUser lUser = getLoginUser(loginUser);
			String appId = lUser.getAppid();
			Connection conn = ConnectionManager.getConnection();
			String sql = "SELECT * FROM ai_res_own_relation t1,ai_resource t2,ai_resource t3 WHERE t1.resId1=t2.resId AND t1.resId2=t3.resId AND t2.resCode=? AND t3.resCode=?";
			PreparedStatement ps = null;
			ResultSet rs = null;
			if (lResult.getCode() != null && !"".equals(lResult.getCode())
					&& lResult.getCode().equals("0")) {
				try {
					Resource res1 = isHasRes(appId, resCode1);
					Resource res2 = isHasRes(appId, resCode2);
					String resState1 = res1.getState();
					String resState2 = res2.getState();

					if (!"".equals(resState1) && !"".equals(resState2)) {
						if ("1".equals(resState1) && "1".equals(resState2)) {
							ps = conn.prepareStatement(sql);
							ps.setString(1, resCode1);
							ps.setString(2, resCode2);
							rs = ps.executeQuery();
							if (rs.next()) {
								cResult.setCode(0);
								cResult.setMsg("两个资源必须同时申请！");
							} else {
								cResult.setCode(1);
								cResult.setMsg("两个资源没有关联关系，可以单独申请");
							}
						} else {
							cResult.setCode(2);
							cResult.setMsg("查询的资源中有未启用资源");
						}
					} else {

						cResult.setCode(3);
						cResult.setMsg("查询的资源中有未注册的资源");
					}

				} catch (Exception e) {
				} finally {
					JdbcUtils.closeResultSet(rs);
					JdbcUtils.closeStatement(ps);
				}
			}
			Subject currentUser = SecurityUtils.getSubject();
			// LoginResult logout =
			// logout(loginUser.getDomain(),loginUser.getAccountCode());
			currentUser.logout();
			return cResult;
		} else {
			return null;
		}
	}

	/**
	 * 查询某个资源所属的组织
	 * 
	 * @param LoginUser
	 * @param resCode
	 * 
	 */
	public OrgResult queryResOrg(LoginUser loginUser, String resCode) {

		if (TokenUtil.validateSignature(signature, timestamp, nonce)) {

			OrgResult oResult = new OrgResult();
			Connection conn = ConnectionManager.getConnection();
			PreparedStatement ps = null;
			ResultSet rs = null;
			LoginResult lResult = login(loginUser);// 登录

			LoginUser lUser = getLoginUser(loginUser);
			String appId = lUser.getAppid();
			List<Org> orgs = new ArrayList<Org>();
			String sql = "SELECT * FROM ai_org t1,ai_org_res t2,ai_resource t3 WHERE t1.orgId=t2.orgId AND t2.resId=t3.resId AND t3.resCode=?";
			if (lResult.getCode() != null && !"".equals(lResult.getCode())
					&& lResult.getCode().equals("0")) {

				try {
					Resource resource = isHasRes(appId, resCode);
					if (resource != null) {
						if (resource.getState().equals("1")) {
							ps = conn.prepareStatement(sql);
							ps.setString(1, resCode);
							rs = ps.executeQuery();
							while (rs.next()) {
								Org org = new Org();
								org.setCreateDate(rs.getDate("createDate"));
								org.setDomain(rs.getString("domain"));
								org.setId(rs.getInt("id"));
								org.setOrgCode(rs.getString("orgCode"));
								org.setOrgId(rs.getString("orgId"));
								org.setOrgName(rs.getString("orgName"));
								org.setState(rs.getString("state"));
								orgs.add(org);
							}
							if (orgs.size() > 0) {
								oResult.setCode(0);
								oResult.setMsg("已获取该资源所属的组织信息");
								oResult.setOrgs(orgs);
							} else {
								oResult.setCode(1);
								oResult.setMsg("该资源没有授权给任何组织");

							}
						} else {

							oResult.setCode(2);
							oResult.setMsg("该资源未启用");
						}
					} else {

						oResult.setCode(3);
						oResult.setMsg("该资源未注册");
					}
				} catch (Exception e) {
				} finally {
					JdbcUtils.closeResultSet(rs);
					JdbcUtils.closeStatement(ps);
				}
			}
			Subject currentUser = SecurityUtils.getSubject();
			// LoginResult logout =
			// logout(loginUser.getDomain(),loginUser.getAccountCode());
			currentUser.logout();
			return oResult;
		} else {
			return null;
		}

	}

	/**
	 * 查询某个资源所属的应用
	 * 
	 * @param LoginUser
	 * @param resCode
	 * 
	 */
	public AppResult queryResApp(LoginUser loginUser, String resCode) {

		if (TokenUtil.validateSignature(signature, timestamp, nonce)) {

			AppResult aResult = new AppResult();
			Connection conn = ConnectionManager.getConnection();
			PreparedStatement ps = null;
			ResultSet rs = null;
			LoginResult lResult = login(loginUser);// 登录

			LoginUser lUser = getLoginUser(loginUser);
			List<App> apps = new ArrayList<App>();
			String sql = "SELECT * FROM ai_app t1,ai_resource t2 WHERE t1.appId=t2.appId AND t2.resCode=?";
			if (lResult.getCode() != null && !"".equals(lResult.getCode())
					&& lResult.getCode().equals("0")) {

				try {
					ps = conn.prepareStatement(sql);
					ps.setString(1, resCode);
					rs = ps.executeQuery();
					while (rs.next()) {
						App app = new App();
						app.setAppCode(rs.getString("appCode"));
						app.setAppid(rs.getString("appid"));
						app.setAppName(rs.getString("appName"));
						app.setCreateDate(rs.getDate("createDate"));
						app.setState(rs.getString("state"));
						app.setId(rs.getInt("id"));
						apps.add(app);

					}
					if (apps.size() > 0) {
						aResult.setCode(0);
						aResult.setMsg("已获取该资源所属的应用信息");
						aResult.setApps(apps);
					} else {
						aResult.setCode(1);
						aResult.setMsg("该资源未注册或者没有该资源的应用信息");

					}
				} catch (Exception e) {
				} finally {
					JdbcUtils.closeResultSet(rs);
					JdbcUtils.closeStatement(ps);
				}
			}
			Subject currentUser = SecurityUtils.getSubject();
			// LoginResult logout =
			// logout(loginUser.getDomain(),loginUser.getAccountCode());
			currentUser.logout();
			return aResult;
		} else {
			return null;
		}
	}

	/**
	 * 查询资源所属角色
	 * 
	 * @param loginUser
	 * @param resCode
	 * 
	 */
	public RoleResult queryResRole(LoginUser loginUser, String resCode) {

		if (TokenUtil.validateSignature(signature, timestamp, nonce)) {

			RoleResult rResult = new RoleResult();
			Connection conn = ConnectionManager.getConnection();
			PreparedStatement ps = null;
			ResultSet rs = null;
			LoginResult lResult = login(loginUser);// 登录
			List<Role> roles = new ArrayList<Role>();
			LoginUser lUser = getLoginUser(loginUser);
			String domain = lUser.getDomain();
			String appid = lUser.getAppid();
			String sql = "select * from ai_role_res t1,ai_roles t2,ai_resource t3 where t1.roleId=t2.roleId and t1.resId=t3.resId and t3.resCode=? and t1.orgId=? and t2.appId=?";
			if (lResult.getCode() != null && !"".equals(lResult.getCode())
					&& lResult.getCode().equals("0")) {

				try {
					Resource res = isHasRes(appid, resCode);
					if (res != null) {
						String orgid = queryOrgByDomain(domain);
						ps = conn.prepareStatement(sql);
						ps.setString(1, resCode);
						ps.setString(2, orgid);
						ps.setString(3, appid);
						rs = ps.executeQuery();
						while (rs.next()) {
							Role role = new Role();
							role.setAppId(rs.getString("appId"));
							role.setCreateDate(rs.getDate("createDate"));
							role.setDescriptionx(rs.getString("descriptionx"));
							role.setId(rs.getInt("id"));
							role.setOrgId(rs.getString("orgId"));
							role.setRoleId(rs.getString("roleId"));
							role.setRoleName(rs.getString("roleName"));
							role.setState(rs.getString("state"));
							roles.add(role);
						}
						if (roles.size() > 0) {
							rResult.setCode(0);
							rResult.setMsg("获取该资源所属的角色");
							rResult.setRole(roles);
						} else {
							rResult.setCode(1);
							rResult.setMsg("该资源没有授权给任何角色");
						}
					} else {

						rResult.setCode(2);
						rResult.setMsg("该资源不存在！");
					}
				} catch (Exception e) {
				} finally {
					JdbcUtils.closeResultSet(rs);
					JdbcUtils.closeStatement(ps);
				}
			}
			Subject currentUser = SecurityUtils.getSubject();
			// LoginResult logout =
			// logout(loginUser.getDomain(),loginUser.getAccountCode());
			currentUser.logout();
			return rResult;
		} else {
			return null;
		}
	}

	/**
	 * 查询资源所属账号
	 * 
	 * @param loginUser
	 * @param resCode
	 * 
	 */
	public AccountResultbak queryResAccount(LoginUser loginUser, String resCode) {

		if (TokenUtil.validateSignature(signature, timestamp, nonce)) {

			AccountResultbak aResult = new AccountResultbak();
			Connection conn = ConnectionManager.getConnection();
			PreparedStatement ps = null;
			ResultSet rs = null;
			LoginResult lResult = login(loginUser);// 登录
			List<User> accs = new ArrayList<User>();
			LoginUser lUser = getLoginUser(loginUser);
			String domain = lUser.getDomain();
			String appid = lUser.getAppid();
			String sql = "SELECT t5.* FROM ai_role_res t1,ai_roles t2,ai_resource t3,ai_account_role t4,ai_account t5 WHERE t1.roleId=t2.roleId AND t1.resId=t3.resId AND t2.roleId=t4.roleId AND t4.accountId=t5.accountId AND t3.resCode=? AND t1.orgId=? AND t2.appId=?";
			if (lResult.getCode() != null && !"".equals(lResult.getCode())
					&& lResult.getCode().equals("0")) {

				try {
					Resource res = isHasRes(appid, resCode);
					if (res != null) {
						String orgid = queryOrgByDomain(domain);

						ps = conn.prepareStatement(sql);
						ps.setString(1, resCode);
						ps.setString(2, orgid);
						ps.setString(3, appid);
						rs = ps.executeQuery();
						while (rs.next()) {
							User accUser = new User();
							accUser.setAccountCode(rs.getString("accountCode"));
							accUser.setAccountName(rs.getString("accountName"));
							accs.add(accUser);
						}
						if (accs.size() > 0) {
							aResult.setCode(0);
							aResult.setMsg("已获取该资源所属用户信息");
							aResult.setAccUsers(accs);
						} else {
							aResult.setCode(1);
							aResult.setMsg("该资源没有授权给任何用户");
						}
					} else {
						aResult.setCode(2);
						aResult.setMsg("该资源不存在");

					}
				} catch (Exception e) {
				} finally {
					JdbcUtils.closeResultSet(rs);
					JdbcUtils.closeStatement(ps);
				}
			}
			Subject currentUser = SecurityUtils.getSubject();
			currentUser.logout();
			return aResult;
		} else {
			return null;
		}
	}

	/*
	 * 查询部门详细信息
	 * 
	 * @param loginUser
	 * 
	 * @param deptName
	 */
	public DeptResult queryDeptDetail(LoginUser loginUser, String deptName) {

		if (TokenUtil.validateSignature(signature, timestamp, nonce)) {

			DeptResult dResult = new DeptResult();
			Connection conn = ConnectionManager.getConnection();
			PreparedStatement ps = null;
			ResultSet rs = null;
			LoginResult lResult = login(loginUser);// 登录
			List<Dept> depts = new ArrayList<Dept>();
			LoginUser lUser = getLoginUser(loginUser);
			String domain = lUser.getDomain();
			String sql = "select * from ai_dept where orgId=? and deptName like ?";
			if (lResult.getCode() != null && !"".equals(lResult.getCode())
					&& lResult.getCode().equals("0")) {

				try {
					String orgId = queryOrgByDomain(domain);
					ps = conn.prepareStatement(sql);
					ps.setString(1, orgId);
					ps.setString(2, "%" + deptName + "%");
					rs = ps.executeQuery();
					while (rs.next()) {
						Dept dept = new Dept();
						dept.setCreateDate(rs.getDate("createDate"));
						dept.setDeptId(rs.getString("deptId"));
						dept.setDeptName(rs.getString("deptName"));
						dept.setId(rs.getInt("id"));
						dept.setOrgId(rs.getString("orgId"));
						dept.setState(rs.getString("state"));
						dept.setUpDeptId(rs.getString("upDeptId"));
						depts.add(dept);
					}
					if (depts.size() > 0) {
						dResult.setCode(0);
						dResult.setMsg("部门信息获取成功！");
						dResult.setDepts(depts);
					} else {
						dResult.setCode(1);
						dResult.setMsg("没有该部门！");

					}
				} catch (Exception e) {
				} finally {
					JdbcUtils.closeResultSet(rs);
					JdbcUtils.closeStatement(ps);
				}
			}
			Subject currentUser = SecurityUtils.getSubject();
			currentUser.logout();
			return dResult;
		} else {
			return null;
		}
	}

	/*
	 * 查询部门的上级部门信息
	 * 
	 * @param loginUser
	 * 
	 * @param deptId
	 */
	public DeptResult queryUpDept(LoginUser loginUser, String deptId) {

		if (TokenUtil.validateSignature(signature, timestamp, nonce)) {

			DeptResult dResult = new DeptResult();
			Connection conn = ConnectionManager.getConnection();
			PreparedStatement ps = null;
			ResultSet rs = null;
			LoginResult lResult = login(loginUser);// 登录

			LoginUser lUser = getLoginUser(loginUser);
			String domain = lUser.getDomain();

			List<Dept> depts = new ArrayList<Dept>();
			String sql = "SELECT * FROM ai_dept t1 WHERE t1.`deptId` IN (SELECT t.`upDeptId` FROM ai_dept t WHERE t.orgId=? and t.deptId=? ) ";
			if (lResult.getCode() != null && !"".equals(lResult.getCode())
					&& lResult.getCode().equals("0")) {

				try {
					String orgId = queryOrgByDomain(domain);
					ps = conn.prepareStatement(sql);
					ps.setString(1, orgId);
					ps.setString(2, deptId);
					rs = ps.executeQuery();
					while (rs.next()) {
						Dept dept = new Dept();
						dept.setCreateDate(rs.getDate("createDate"));
						dept.setDeptId(rs.getString("deptId"));
						dept.setDeptName(rs.getString("deptName"));
						dept.setId(rs.getInt("id"));
						dept.setOrgId(rs.getString("orgId"));
						dept.setState(rs.getString("state"));
						dept.setUpDeptId(rs.getString("upDeptId"));
						depts.add(dept);
					}
					if (depts.size() > 0) {
						dResult.setCode(0);
						dResult.setMsg("上级部门信息获取成功！");
						dResult.setDepts(depts);
					} else {
						dResult.setCode(1);
						dResult.setMsg("没有上级部门！");

					}
				} catch (Exception e) {
				} finally {
					JdbcUtils.closeResultSet(rs);
					JdbcUtils.closeStatement(ps);
				}
			}
			Subject currentUser = SecurityUtils.getSubject();
			currentUser.logout();
			return dResult;
		} else {
			return null;
		}

	}

	/*
	 * 查询下级部门信息
	 * 
	 * @param loginUser
	 * 
	 * @param deptId
	 */
	public DeptResult queryNextDept(LoginUser loginUser, String deptId) {

		if (TokenUtil.validateSignature(signature, timestamp, nonce)) {

			DeptResult dResult = new DeptResult();
			Connection conn = ConnectionManager.getConnection();
			PreparedStatement ps = null;
			ResultSet rs = null;
			LoginResult lResult = login(loginUser);// 登录

			LoginUser lUser = getLoginUser(loginUser);
			String domain = lUser.getDomain();

			List<Dept> depts = new ArrayList<Dept>();
			String sql = "SELECT * FROM ai_dept t1 WHERE t1.orgId=?  and t1.`upDeptId`=? ";
			if (lResult.getCode() != null && !"".equals(lResult.getCode())
					&& lResult.getCode().equals("0")) {

				try {
					String orgId = queryOrgByDomain(domain);
					ps = conn.prepareStatement(sql);
					ps.setString(1, orgId);
					ps.setString(2, deptId);
					rs = ps.executeQuery();
					while (rs.next()) {
						Dept dept = new Dept();
						dept.setCreateDate(rs.getDate("createDate"));
						dept.setDeptId(rs.getString("deptId"));
						dept.setDeptName(rs.getString("deptName"));
						dept.setId(rs.getInt("id"));
						dept.setOrgId(rs.getString("orgId"));
						dept.setState(rs.getString("state"));
						dept.setUpDeptId(rs.getString("upDeptId"));
						depts.add(dept);
					}
					if (depts.size() > 0) {
						dResult.setCode(0);
						dResult.setMsg("下部门信息获取成功！");
						dResult.setDepts(depts);
					} else {
						dResult.setCode(1);
						dResult.setMsg("没有下级部门！");

					}
				} catch (Exception e) {
				} finally {
					JdbcUtils.closeResultSet(rs);
					JdbcUtils.closeStatement(ps);
				}
			}
			Subject currentUser = SecurityUtils.getSubject();
			currentUser.logout();
			return dResult;
		} else {
			return null;
		}
	}

	/*
	 * 查询部门内的人员
	 * 
	 * @param loginUser
	 * 
	 * @param deptId
	 */

	public AccountResultbak queryDeptAccount(LoginUser loginUser, String deptId) {

		if (TokenUtil.validateSignature(signature, timestamp, nonce)) {

			AccountResultbak aResult = new AccountResultbak();
			Connection conn = ConnectionManager.getConnection();
			PreparedStatement ps = null;
			ResultSet rs = null;
			LoginResult lResult = login(loginUser);// 登录

			LoginUser lUser = getLoginUser(loginUser);
			String domain = lUser.getDomain();
			List<User> accUsers = new ArrayList<User>();
			String sql = "select * from ai_account t where t.deptId=? and t.orgId=?";
			if (lResult.getCode() != null && !"".equals(lResult.getCode())
					&& lResult.getCode().equals("0")) {
				try {
					String orgId = queryOrgByDomain(domain);
					ps = conn.prepareStatement(sql);
					ps.setString(1, deptId);
					ps.setString(2, orgId);
					rs = ps.executeQuery();
					while (rs.next()) {
						User user = new User();
						user.setAccountCode(rs.getString("accountCode"));
						user.setAccountName(rs.getString("accountName"));
						accUsers.add(user);
					}
					if (accUsers.size() > 0) {
						aResult.setCode(0);
						aResult.setMsg("已获取账号信息");
						aResult.setAccUsers(accUsers);
					} else {
						aResult.setCode(1);
						aResult.setMsg("该部门下没有人员");

					}
				} catch (Exception e) {
				} finally {
					JdbcUtils.closeResultSet(rs);
					JdbcUtils.closeStatement(ps);
				}
			}
			Subject currentUser = SecurityUtils.getSubject();
			currentUser.logout();
			return aResult;

		} else {
			return null;
		}
	}

	/*
	 * 查询某个部门是否被禁用
	 * 
	 * @param loginUser
	 * 
	 * @param deptId
	 */
	public CommonResult queryDeptState(LoginUser loginUser, String deptId) {

		if (TokenUtil.validateSignature(signature, timestamp, nonce)) {

			CommonResult cResult = new CommonResult();
			Connection conn = ConnectionManager.getConnection();
			PreparedStatement ps = null;
			ResultSet rs = null;
			LoginResult lResult = login(loginUser);// 登录

			LoginUser lUser = getLoginUser(loginUser);
			String domain = lUser.getDomain();
			String sql = "select * from ai_dept where deptId=? and orgId=?";
			if (lResult.getCode() != null && !"".equals(lResult.getCode())
					&& lResult.getCode().equals("0")) {
				try {
					String orgId = queryOrgByDomain(domain);
					ps = conn.prepareStatement(sql);
					ps.setString(1, deptId);
					ps.setString(2, orgId);
					rs = ps.executeQuery();
					if (rs.next()) {
						if (rs.getString("state") != null
								&& !"".equals(rs.getString("state"))) {
							if ("1".equals(rs.getString("state"))) {
								cResult.setCode(0);
								cResult.setMsg("该部门已被启用");
							} else {
								cResult.setCode(1);
								cResult.setMsg("该部门已被禁用");

							}
						}
					} else {
						cResult.setCode(2);
						cResult.setMsg("该部门不存在");
					}
				} catch (Exception e) {
				} finally {
					JdbcUtils.closeResultSet(rs);
					JdbcUtils.closeStatement(ps);
				}
			}
			Subject currentUser = SecurityUtils.getSubject();
			currentUser.logout();
			return cResult;
		} else {
			return null;
		}
	}

	/*
	 * 查询登录账号所属组织详细信息
	 * 
	 * @param loginuser
	 */
	public OrgResult queryOrgDetail(LoginUser loginUser) {

		if (TokenUtil.validateSignature(signature, timestamp, nonce)) {

			OrgResult oResult = new OrgResult();
			Connection conn = ConnectionManager.getConnection();
			PreparedStatement ps = null;
			ResultSet rs = null;
			LoginResult lResult = login(loginUser);// 登录

			LoginUser lUser = getLoginUser(loginUser);
			String domain = lUser.getDomain();
			List<Org> orgs = new ArrayList<Org>();
			String sql = "select * from ai_org where  domain=?";
			if (lResult.getCode() != null && !"".equals(lResult.getCode())
					&& lResult.getCode().equals("0")) {
				try {
					ps = conn.prepareStatement(sql);
					ps.setString(1, domain);
					rs = ps.executeQuery();
					while (rs.next()) {
						Org org = new Org();
						org.setCreateDate(rs.getDate("createDate"));
						org.setDomain(domain);
						org.setId(rs.getInt("id"));
						org.setOrgCode(rs.getString("orgCode"));
						org.setOrgId(rs.getString("orgId"));
						org.setOrgName(rs.getString("orgName"));
						org.setState(rs.getString("state"));
						orgs.add(org);
					}
					if (orgs.size() > 0) {
						oResult.setCode(0);
						oResult.setMsg("已获取组织详细信息");
						oResult.setOrgs(orgs);
					} else {
						oResult.setCode(1);
						oResult.setMsg("没有该组织信息");
					}
				} catch (Exception e) {
				} finally {
					JdbcUtils.closeResultSet(rs);
					JdbcUtils.closeStatement(ps);
				}
			}
			Subject currentUser = SecurityUtils.getSubject();
			currentUser.logout();
			return oResult;
		} else {
			return null;
		}

	}

	/*
	 * 查询登录账号所属组织详细信息
	 * 
	 * @param loginuser
	 */
	public AppResult queryOrgApps(LoginUser loginUser) {

		if (TokenUtil.validateSignature(signature, timestamp, nonce)) {

			AppResult aResult = new AppResult();
			Connection conn = ConnectionManager.getConnection();
			PreparedStatement ps = null;
			ResultSet rs = null;
			LoginResult lResult = login(loginUser);// 登录

			LoginUser lUser = getLoginUser(loginUser);
			String domain = lUser.getDomain();
			List<App> apps = new ArrayList<App>();
			String sql = "SELECT * FROM ai_app t1 ,ai_org_app t2 WHERE t1.`appId`=t2.`appId` AND t2.`orgId`=?";
			if (lResult.getCode() != null && !"".equals(lResult.getCode())
					&& lResult.getCode().equals("0")) {
				try {
					String orgid = queryOrgByDomain(domain);
					ps = conn.prepareStatement(sql);
					ps.setString(1, orgid);
					rs = ps.executeQuery();
					while (rs.next()) {
						App app = new App();
						app.setAppCode(rs.getString("appCode"));
						app.setAppid(rs.getString("appid"));
						app.setAppName(rs.getString("appName"));
						app.setCreateDate(rs.getDate("createDate"));
						app.setId(rs.getInt("id"));
						app.setState(rs.getString("state"));
						apps.add(app);
					}
					if (apps.size() > 0) {
						aResult.setCode(0);
						aResult.setMsg("已获取组织的应用信息");
						aResult.setApps(apps);
					} else {
						aResult.setCode(1);
						aResult.setMsg("该组织没有申请应用");
					}
				} catch (Exception e) {
				} finally {
					JdbcUtils.closeResultSet(rs);
					JdbcUtils.closeStatement(ps);
				}
			}
			Subject currentUser = SecurityUtils.getSubject();
			currentUser.logout();
			return aResult;
		} else {
			return null;
		}

	}

	/*
	 * 查询账号所在组织的所有资源
	 */
	public ResourceResult queryOrgResources(LoginUser loginUser) {

		if (TokenUtil.validateSignature(signature, timestamp, nonce)) {

			ResourceResult rResult = new ResourceResult();
			Connection conn = ConnectionManager.getConnection();
			PreparedStatement ps = null;
			ResultSet rs = null;
			LoginResult lResult = login(loginUser);// 登录

			LoginUser lUser = getLoginUser(loginUser);
			String domain = lUser.getDomain();
			List<Resource> resources = new ArrayList<Resource>();
			String sql = "SELECT * FROM ai_org_res t1 ,ai_resource t2 WHERE t1.`resId`=t2.`resId` AND t1.`orgId`=?";
			if (lResult.getCode() != null && !"".equals(lResult.getCode())
					&& lResult.getCode().equals("0")) {
				try {
					String orgid = queryOrgByDomain(domain);
					ps = conn.prepareStatement(sql);
					ps.setString(1, orgid);
					rs = ps.executeQuery();
					while (rs.next()) {
						Resource resource = new Resource();
						resource.setAppId(rs.getString("appId"));
						resource.setCreateDate(rs.getDate("createDate"));
						resource.setId(rs.getInt("id"));
						resource.setResCode(rs.getString("resCode"));
						resource.setResContent(rs.getString("resContent"));
						resource.setResDes(rs.getString("resDes"));
						resource.setResId(rs.getString("resId"));
						resource.setResName(rs.getString("resName"));
						resource.setState(rs.getString("state"));
						resource.setType(rs.getString("type"));
						resources.add(resource);
					}
					if (resources.size() > 0) {
						rResult.setCode(0);
						rResult.setMsg("已获取组织的资源信息");
						rResult.setResources(resources);
					} else {
						rResult.setCode(1);
						rResult.setMsg("该组织没有申请应用");
					}
				} catch (Exception e) {
				} finally {
					JdbcUtils.closeResultSet(rs);
					JdbcUtils.closeStatement(ps);
				}
			}
			Subject currentUser = SecurityUtils.getSubject();
			currentUser.logout();
			return rResult;
		} else {
			return null;
		}

	}

	/*
	 * 查询账号所在组织的部门信息
	 */
	public DeptResult queryOrgDepts(LoginUser loginUser) {

		if (TokenUtil.validateSignature(signature, timestamp, nonce)) {

			DeptResult dResult = new DeptResult();
			Connection conn = ConnectionManager.getConnection();
			PreparedStatement ps = null;
			ResultSet rs = null;
			LoginResult lResult = login(loginUser);// 登录

			LoginUser lUser = getLoginUser(loginUser);
			String domain = lUser.getDomain();
			List<Dept> depts = new ArrayList<Dept>();
			String sql = "SELECT * FROM ai_dept WHERE `orgId`=?";
			if (lResult.getCode() != null && !"".equals(lResult.getCode())
					&& lResult.getCode().equals("0")) {
				try {
					String orgid = queryOrgByDomain(domain);
					ps = conn.prepareStatement(sql);
					ps.setString(1, orgid);
					rs = ps.executeQuery();
					while (rs.next()) {
						Dept dept = new Dept();
						dept.setCreateDate(rs.getDate("createDate"));
						dept.setDeptId(rs.getString("deptId"));
						dept.setDeptName(rs.getString("deptName"));
						dept.setId(rs.getInt("id"));
						dept.setOrgId(rs.getString("orgId"));
						dept.setState(rs.getString("state"));
						dept.setUpDeptId(rs.getString("upDeptId"));
						depts.add(dept);
					}
					if (depts.size() > 0) {
						dResult.setCode(0);
						dResult.setMsg("已获取组织的应用信息");
						dResult.setDepts(depts);
					} else {
						dResult.setCode(1);
						dResult.setMsg("该组织没有申请应用");
					}
				} catch (Exception e) {
				} finally {
					JdbcUtils.closeResultSet(rs);
					JdbcUtils.closeStatement(ps);
				}
			}
			Subject currentUser = SecurityUtils.getSubject();
			currentUser.logout();
			return dResult;
		} else {
			return null;
		}

	}

	/*
	 * 查询账号所在组织的部门信息
	 */
	public AccountResultbak queryOrgAccount(LoginUser loginUser) {

		if (TokenUtil.validateSignature(signature, timestamp, nonce)) {

			AccountResultbak aResult = new AccountResultbak();
			Connection conn = ConnectionManager.getConnection();
			PreparedStatement ps = null;
			ResultSet rs = null;
			LoginResult lResult = login(loginUser);// 登录

			LoginUser lUser = getLoginUser(loginUser);
			String domain = lUser.getDomain();
			List<User> users = new ArrayList<User>();
			String sql = "SELECT * FROM ai_account WHERE `orgId`=?";
			if (lResult.getCode() != null && !"".equals(lResult.getCode())
					&& lResult.getCode().equals("0")) {
				try {
					String orgid = queryOrgByDomain(domain);
					ps = conn.prepareStatement(sql);
					ps.setString(1, orgid);
					rs = ps.executeQuery();
					while (rs.next()) {
						User user = new User();
						user.setAccountCode(rs.getString("accountCode"));
						user.setAccountName(rs.getString("accountName"));
						users.add(user);
					}
					if (users.size() > 0) {
						aResult.setCode(0);
						aResult.setMsg("已获取组织的人员信息");
						aResult.setAccUsers(users);
					} else {
						aResult.setCode(1);
						aResult.setMsg("该组织内没有用户信息");
					}
				} catch (Exception e) {
				} finally {
					JdbcUtils.closeResultSet(rs);
					JdbcUtils.closeStatement(ps);
				}
			}
			Subject currentUser = SecurityUtils.getSubject();
			currentUser.logout();
			return aResult;
		} else {
			return null;
		}

	}

	// 查询组织状态
	public CommonResult queryOrgState(LoginUser loginUser) {

		if (TokenUtil.validateSignature(signature, timestamp, nonce)) {

			CommonResult cResult = new CommonResult();
			Connection conn = ConnectionManager.getConnection();
			PreparedStatement ps = null;
			ResultSet rs = null;
			LoginResult lResult = login(loginUser);// 登录

			LoginUser lUser = getLoginUser(loginUser);
			String domain = lUser.getDomain();
			List<User> users = new ArrayList<User>();
			String sql = "SELECT * FROM ai_org WHERE domain=?";
			if (lResult.getCode() != null && !"".equals(lResult.getCode())
					&& lResult.getCode().equals("0")) {
				try {
					ps = conn.prepareStatement(sql);
					ps.setString(1, domain);
					rs = ps.executeQuery();
					if (rs.next()) {
						if (rs.getString("state") != null
								&& !"".equals(rs.getString("state"))) {
							if (rs.getString("state").equals("1")) {
								cResult.setCode(0);
								cResult.setMsg("组织已启用");
							} else {
								cResult.setCode(1);
								cResult.setMsg("组织已被禁用");

							}
						} else {
							cResult.setCode(2);
							cResult.setMsg("该组织状态未设置");
						}
					} else {
						cResult.setCode(3);
						cResult.setMsg("该组织不存在");
					}
				} catch (Exception e) {
				} finally {
					JdbcUtils.closeResultSet(rs);
					JdbcUtils.closeStatement(ps);
				}
			}
			Subject currentUser = SecurityUtils.getSubject();
			currentUser.logout();
			return cResult;
		} else {
			return null;
		}
	}

	// 查询应用详情
	public AppResult queryAppDetail(LoginUser loginUser) {

		if (TokenUtil.validateSignature(signature, timestamp, nonce)) {

			AppResult aResult = new AppResult();
			Connection conn = ConnectionManager.getConnection();
			PreparedStatement ps = null;
			ResultSet rs = null;
			LoginResult lResult = login(loginUser);// 登录

			LoginUser lUser = getLoginUser(loginUser);
			String appid = lUser.getAppid();
			List<App> apps = new ArrayList<App>();
			String sql = "SELECT * FROM ai_app WHERE appId=?";
			if (lResult.getCode() != null && !"".equals(lResult.getCode())
					&& lResult.getCode().equals("0")) {
				try {
					ps = conn.prepareStatement(sql);
					ps.setString(1, appid);
					rs = ps.executeQuery();
					while (rs.next()) {
						App app = new App();
						app.setAppCode(rs.getString("appCode"));
						app.setAppid(rs.getString("appid"));
						app.setAppName(rs.getString("appName"));
						app.setCreateDate(rs.getDate("createDate"));
						app.setId(rs.getInt("id"));
						app.setState(rs.getString("state"));
						apps.add(app);
					}
					if (apps.size() > 0) {
						aResult.setApps(apps);
						aResult.setCode(0);
						aResult.setMsg("已获取应用详细信息");
					} else {
						aResult.setCode(1);
						aResult.setMsg("该应用没有在权限平台注册");

					}
				} catch (Exception e) {
				} finally {
					JdbcUtils.closeResultSet(rs);
					JdbcUtils.closeStatement(ps);
				}
			}
			Subject currentUser = SecurityUtils.getSubject();
			currentUser.logout();
			return aResult;
		} else {
			return null;
		}
	}

	// 查询应用所属组织详情
	public OrgResult queryAppOrg(LoginUser loginUser) {

		if (TokenUtil.validateSignature(signature, timestamp, nonce)) {

			OrgResult oResult = new OrgResult();
			Connection conn = ConnectionManager.getConnection();
			PreparedStatement ps = null;
			ResultSet rs = null;
			LoginResult lResult = login(loginUser);// 登录

			LoginUser lUser = getLoginUser(loginUser);
			String appid = lUser.getAppid();
			List<Org> orgs = new ArrayList<Org>();
			String sql = "SELECT * FROM ai_org t1,ai_org_app t2 WHERE t1.`orgId`=t2.`orgId` AND t2.`appId`=?";
			if (lResult.getCode() != null && !"".equals(lResult.getCode())
					&& lResult.getCode().equals("0")) {
				try {
					ps = conn.prepareStatement(sql);
					ps.setString(1, appid);
					rs = ps.executeQuery();
					while (rs.next()) {
						Org org = new Org();
						org.setCreateDate(rs.getDate("createDate"));
						org.setDomain(rs.getString("domain"));
						org.setId(rs.getInt("id"));
						org.setOrgCode(rs.getString("orgCode"));
						org.setOrgId(rs.getString("orgId"));
						org.setOrgName(rs.getString("orgName"));
						org.setState(rs.getString("state"));
						orgs.add(org);
					}
					if (orgs.size() > 0) {
						oResult.setCode(0);
						oResult.setMsg("已获取该应用所属的组织信息");
						oResult.setOrgs(orgs);
					} else {
						oResult.setCode(1);
						oResult.setMsg("该应用没有授权给任何组织");

					}
				} catch (Exception e) {
				} finally {
					JdbcUtils.closeResultSet(rs);
					JdbcUtils.closeStatement(ps);
				}
			}
			Subject currentUser = SecurityUtils.getSubject();
			currentUser.logout();
			return oResult;
		} else {
			return null;
		}
	}

	// 查询应用的资源
	public ResourceResult queryAppRes(LoginUser loginUser) {

		if (TokenUtil.validateSignature(signature, timestamp, nonce)) {

			ResourceResult rResult = new ResourceResult();
			Connection conn = ConnectionManager.getConnection();
			PreparedStatement ps = null;
			ResultSet rs = null;
			LoginResult lResult = login(loginUser);// 登录

			LoginUser lUser = getLoginUser(loginUser);
			String appid = lUser.getAppid();
			List<Resource> resources = new ArrayList<Resource>();
			String sql = "SELECT * FROM ai_resource WHERE appId=?";
			if (lResult.getCode() != null && !"".equals(lResult.getCode())
					&& lResult.getCode().equals("0")) {
				try {
					ps = conn.prepareStatement(sql);
					ps.setString(1, appid);
					rs = ps.executeQuery();
					while (rs.next()) {
						Resource resource = new Resource();
						resource.setAppId(rs.getString("appId"));
						resource.setCreateDate(rs.getDate("createDate"));
						resource.setId(rs.getInt("id"));
						resource.setResCode(rs.getString("resCode"));
						resource.setResContent(rs.getString("resContent"));
						resource.setResDes(rs.getString("resDes"));
						resource.setResId(rs.getString("resId"));
						resource.setResName(rs.getString("resName"));
						resource.setState(rs.getString("state"));
						resource.setType(rs.getString("type"));
						resources.add(resource);
					}
					if (resources.size() > 0) {
						rResult.setCode(0);
						rResult.setMsg("已获取该应用的资源信息");
						rResult.setResources(resources);
					} else {
						rResult.setCode(1);
						rResult.setMsg("该应用没有注册资源");

					}
				} catch (Exception e) {
				} finally {
					JdbcUtils.closeResultSet(rs);
					JdbcUtils.closeStatement(ps);
				}
			}
			Subject currentUser = SecurityUtils.getSubject();
			currentUser.logout();
			return rResult;
		} else {
			return null;
		}
	}

	// 查询应用对应的角色
	public RoleResult queryAppRoles(LoginUser loginUser) {

		if (TokenUtil.validateSignature(signature, timestamp, nonce)) {

			RoleResult rResult = new RoleResult();
			Connection conn = ConnectionManager.getConnection();
			PreparedStatement ps = null;
			ResultSet rs = null;
			LoginResult lResult = login(loginUser);// 登录

			LoginUser lUser = getLoginUser(loginUser);
			String appid = lUser.getAppid();
			String domain = lUser.getDomain();
			List<Role> roles = new ArrayList<Role>();
			String sql = "SELECT DISTINCT t3.*  FROM ai_resource t1,ai_role_res t2,ai_roles t3 WHERE t1.`resId`=t2.`resId` AND t2.`roleId`=t3.`roleId` AND t1.`appId`=? AND t3.`orgId`=?";
			if (lResult.getCode() != null && !"".equals(lResult.getCode())
					&& lResult.getCode().equals("0")) {
				try {
					String orgid = queryOrgByDomain(domain);
					ps = conn.prepareStatement(sql);
					ps.setString(1, appid);
					ps.setString(2, orgid);
					rs = ps.executeQuery();
					while (rs.next()) {
						Role role = new Role();
						role.setAppId(rs.getString("appId"));
						role.setCreateDate(rs.getDate("createDate"));
						role.setDescriptionx(rs.getString("descriptionx"));
						role.setId(rs.getInt("id"));
						role.setOrgId(rs.getString("orgId"));
						role.setRoleId(rs.getString("roleId"));
						role.setRoleName(rs.getString("roleName"));
						role.setState(rs.getString("state"));
						roles.add(role);
					}
					if (roles.size() > 0) {
						rResult.setCode(0);
						rResult.setMsg("获取该应用的角色");
						rResult.setRole(roles);
					} else {
						rResult.setCode(1);
						rResult.setMsg("该应用没有任何角色");
					}
				} catch (Exception e) {
				} finally {
					JdbcUtils.closeResultSet(rs);
					JdbcUtils.closeStatement(ps);
				}
			}
			Subject currentUser = SecurityUtils.getSubject();
			currentUser.logout();
			return rResult;
		} else {
			return null;
		}
	}

	// 查询应用状态
	public CommonResult queryAppState(LoginUser loginUser) {

		if (TokenUtil.validateSignature(signature, timestamp, nonce)) {

			CommonResult cResult = new CommonResult();
			Connection conn = ConnectionManager.getConnection();
			PreparedStatement ps = null;
			ResultSet rs = null;
			LoginResult lResult = login(loginUser);// 登录

			LoginUser lUser = getLoginUser(loginUser);
			String appid = lUser.getAppid();
			String sql = "select * from ai_app where appId=?";
			if (lResult.getCode() != null && !"".equals(lResult.getCode())
					&& lResult.getCode().equals("0")) {
				try {
					ps = conn.prepareStatement(sql);
					ps.setString(1, appid);
					rs = ps.executeQuery();
					if (rs.next()) {
						if (rs.getString("state") != null
								&& !"".equals(rs.getString("state"))) {
							if ("1".equals(rs.getString("state"))) {
								cResult.setCode(0);
								cResult.setMsg("该应用已启用");
							} else {

								cResult.setCode(1);
								cResult.setMsg("该应用被禁用");
							}
						} else {
							cResult.setCode(2);
							cResult.setMsg("该应用未设置状态");
						}
					} else {
						cResult.setCode(3);
						cResult.setMsg("该应用尚未在权限平台注册");
					}
				} catch (Exception e) {
				} finally {
					JdbcUtils.closeResultSet(rs);
					JdbcUtils.closeStatement(ps);
				}
			}
			Subject currentUser = SecurityUtils.getSubject();
			currentUser.logout();
			return cResult;
		} else {
			return null;
		}
	}

	// //查询是否存在上级部门
	// private String isUpdept(String orgId,String deptName) throws
	// SQLException{
	// String result = "";
	// Connection conn = ConnectionManager.getConnection();
	// String authenticationQuery =
	// "SELECT * FROM ai_dept t WHERE t.orgId=? and t.deptName like ?";
	// PreparedStatement ps = null;
	// ResultSet rs = null;
	// try {
	// ps = conn.prepareStatement(authenticationQuery);
	// ps.setString(1, orgId);
	// ps.setString(2, "%" + deptName + "%");
	// rs = ps.executeQuery();
	// while (rs.next()) {
	// if(rs.getString("upDeptId") != null &&
	// !"".equals(rs.getString("upDeptId"))){
	// result = rs.getString("upDeptId")+",";
	// }
	// }
	// if(!"".equals(result)){
	// result = result.substring(0,result.length()-1);
	// }
	// } finally {
	// JdbcUtils.closeResultSet(rs);
	// JdbcUtils.closeStatement(ps);
	// }
	// return result;
	//
	// }
	// 查询资源是否存在
	private Resource isHasRes(String appId, String resCode) throws SQLException {

		if (TokenUtil.validateSignature(signature, timestamp, nonce)) {

			Resource resource = new Resource();
			Connection conn = ConnectionManager.getConnection();
			String authenticationQuery = "SELECT * FROM ai_resource t WHERE  t.resCode=? AND t.appId=?";
			PreparedStatement ps = null;
			ResultSet rs = null;
			try {
				ps = conn.prepareStatement(authenticationQuery);
				ps.setString(1, resCode);
				ps.setString(2, appId);
				rs = ps.executeQuery();
				while (rs.next()) {
					resource.setResCode(rs.getString("resCode"));
					resource.setResContent(rs.getString("resContent"));
					resource.setResDes(rs.getString("resDes"));
					resource.setResId(rs.getString("resId"));
					resource.setResName(rs.getString("resName"));
					resource.setState(rs.getString("state"));
				}

			} finally {
				JdbcUtils.closeResultSet(rs);
				JdbcUtils.closeStatement(ps);
			}

			return resource;
		} else {
			return null;
		}
	}

	/**
	 * 查询是否存在账号
	 * 
	 * @param orgCode
	 * @param accountCode
	 * @return
	 * @throws SQLException
	 */
	private String isHaveAccount(String orgCode, String accountCode)
			throws SQLException {

		if (TokenUtil.validateSignature(signature, timestamp, nonce)) {

			Connection conn = ConnectionManager.getConnection();
			String authenticationQuery = "SELECT * FROM ai_account WHERE accountCode=? AND orgId=?";
			PreparedStatement ps = null;
			ResultSet rs = null;
			String result = "";
			try {
				ps = conn.prepareStatement(authenticationQuery);
				ps.setString(1, accountCode);
				ps.setString(2, orgCode);
				rs = ps.executeQuery();
				while (rs.next()) {
					result = String.valueOf(rs.getString("accountId"));
				}

			} finally {
				JdbcUtils.closeResultSet(rs);
				JdbcUtils.closeStatement(ps);
			}

			return result;
		} else {
			return null;
		}
	}

	/**
	 * 查询密码
	 * 
	 * @param orgCode
	 * @param accountCode
	 * @return
	 * @throws SQLException
	 */
	private String queryOrgByDomain(String domain) throws SQLException {

		if (TokenUtil.validateSignature(signature, timestamp, nonce)) {

			String orgId = "";
			Connection conn = ConnectionManager.getConnection();
			String authenticationQuery = "SELECT * FROM ai_org WHERE domain=?";
			PreparedStatement ps = null;
			ResultSet rs = null;

			try {
				ps = conn.prepareStatement(authenticationQuery);
				ps.setString(1, domain);
				rs = ps.executeQuery();
				while (rs.next()) {
					orgId = rs.getString("orgId");
				}

			} finally {
				JdbcUtils.closeResultSet(rs);
				JdbcUtils.closeStatement(ps);
			}

			return orgId;
		} else {
			return null;
		}
	}

	/**
	 * 查询密码
	 * 
	 * @param orgCode
	 * @param accountCode
	 * @return
	 * @throws SQLException
	 */
	private AIUser queryPwdByAccount(String orgCode, String accountCode)
			throws SQLException {

		if (TokenUtil.validateSignature(signature, timestamp, nonce)) {

			AIUser rUser = new AIUser();
			Connection conn = ConnectionManager.getConnection();
			String authenticationQuery = "SELECT * FROM ai_account WHERE accountCode=? AND orgId=?";
			PreparedStatement ps = null;
			ResultSet rs = null;

			try {
				ps = conn.prepareStatement(authenticationQuery);
				ps.setString(1, accountCode);
				ps.setString(2, orgCode);
				rs = ps.executeQuery();
				while (rs.next()) {
					rUser.setPassword(rs.getString("password"));
					rUser.setAccountId(rs.getString("accountId"));
				}

			} finally {
				JdbcUtils.closeResultSet(rs);
				JdbcUtils.closeStatement(ps);
			}

			return rUser;
		} else {
			return null;
		}
	}

	private boolean isHaveOrg(String orgCode, String orgName) {

		if (TokenUtil.validateSignature(signature, timestamp, nonce)) {

			AIOrg org = new AIOrg();
			Connection conn = ConnectionManager.getConnection();
			String authenticationQuery = "SELECT * FROM ai_org WHERE orgId=? or orgName=?";
			PreparedStatement ps = null;
			ResultSet rs = null;

			try {
				ps = conn.prepareStatement(authenticationQuery);
				ps.setString(1, orgCode);
				ps.setString(2, orgName);
				rs = ps.executeQuery();
				while (rs.next()) {
					org.setOrgCode(rs.getString("orgCode"));
					org.setOrgId(rs.getString("orgId"));
					org.setOrgName(rs.getString("orgName"));
				}

			} catch (Exception e) {

			} finally {
				JdbcUtils.closeResultSet(rs);
				JdbcUtils.closeStatement(ps);
			}

			return org.getOrgId() == null;
		} else {
			return false;
		}
	}

	public LoginUser getLoginUser(LoginUser loginUser) {
		return loginUser;

	}

	@Override
	public boolean doGet(String accountCode,String Message)	throws ServletException, IOException {
		// TODO Auto-generated method stub
		if (TokenUtil.validateSignature(signature, timestamp, nonce)) {
			Myservlet ms=new Myservlet();
			ms.doGet(accountCode,Message);
			return true;
		} else {
			return false;
		}
		
	}
	
	public boolean doGroupGet(List<String> accountCodes,String Message)	throws ServletException, IOException {
		// TODO Auto-generated method stub
		if (TokenUtil.validateSignature(signature, timestamp, nonce)) {
			for(int i=0;i<accountCodes.size();i++){
				Myservlet ms=new Myservlet();
				ms.doGet(accountCodes.get(i),Message);	
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return true;
		} else {
			return false;
		}
		
	}


	public boolean doGroupArrGet(String[] accountCodesArr,String Message)	throws ServletException, IOException{
		// TODO Auto-generated method stub
//		if (TokenUtil.validateSignature(signature, timestamp, nonce)) {
			 
		StringBuffer sb = new StringBuffer();
		for(int i = 0; i < accountCodesArr.length; i++){
		 sb. append(accountCodesArr[i]).append(",");
		}
		String s = sb.toString();	
	    System.out.println("s:"+s);	
		Runtime.getRuntime().exec(ProUtil.propertie.get("url")+"&accountCodeArr="+ s +"&chat="+ URLEncoder.encode( Message, "UTF-8"));
			
			return true;
//		} else {
//			return false;
//		}
		
	}

}
