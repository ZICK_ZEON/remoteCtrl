package com.hession.service.impl;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.hessian.token.TokenUtil;
import com.hessian.token.util.ProUtil;
import com.hession.back.AccountResult;
import com.hession.back.AccountResultbak;
import com.hession.back.AppResult;
import com.hession.back.CommonResult;
import com.hession.back.DeptResult;
import com.hession.back.LoginResult;
import com.hession.back.OrgResult;
import com.hession.back.RegisterResult;
import com.hession.back.ResourceResult;
import com.hession.back.RoleResult;
import com.hession.domain.Account;
import com.hession.domain.LoginUser;
import com.hession.service.DFRightService;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import javax.servlet.ServletException;

public class DFRightServiceImpl implements DFRightService
{
  private String signature;
  private String nonce;
  private String timestamp;
  
  public void setSinagure(String sinature)
  {
    this.signature = sinature;
  }
  
  public void setNonce(String nonce)
  {
    this.nonce = nonce;
  }
  
  public void setTimestamp(String timestamp)
  {
    this.timestamp = timestamp;
  }
  
  public boolean doGroupArrGet(String[] accountCodesArr, String Message)
    throws ServletException, IOException
  {
    if (!TokenUtil.validateSignature(this.signature, this.timestamp, this.nonce)) {
      return false;
    }
    List<AccountResult> userList = new ArrayList();
    for (int i = 0; i < accountCodesArr.length; i++)
    {
      AccountResult account = new AccountResult();
      account.setCode(accountCodesArr[i]);
      userList.add(account);
    }
    Gson gon = new GsonBuilder().create();
    
    Runtime.getRuntime().exec(
      ProUtil.propertie.get("url") + "&accountCodeArr=" + 
      URLEncoder.encode(gon.toJson(userList), "utf-8") + 
      "&chat=" + URLEncoder.encode(Message, "UTF-8"));
    
//    Runtime.getRuntime().exec(ProUtil.propertie.get("url") + "&accountCodeArr=system&chat=" + URLEncoder.encode(Message, "UTF-8"));
    
    return true;
  }

@Override
public RegisterResult registerOrg(String orgCode, String orgName) {
	// TODO Auto-generated method stub
	return null;
}

@Override
public RegisterResult register(String orgId, String account,
		String accountName, String pwd) {
	// TODO Auto-generated method stub
	return null;
}

@Override
public LoginResult login(LoginUser loginUser) {
	// TODO Auto-generated method stub
	return null;
}

@Override
public ResourceResult getRights(LoginUser loginUser) {
	// TODO Auto-generated method stub
	return null;
}

@Override
public CommonResult queryIsUserRes(LoginUser loginUser, List<String> resCode,
		String accountCode) {
	// TODO Auto-generated method stub
	return null;
}

@Override
public CommonResult queryAccountHasRes(LoginUser loginUser, String accountCode,
		String resCode) {
	// TODO Auto-generated method stub
	return null;
}

@Override
public CommonResult createUsers(LoginUser loginUser, List<Account> accs) {
	// TODO Auto-generated method stub
	return null;
}

@Override
public CommonResult queryAccountState(LoginUser loginUser, String accountCode) {
	// TODO Auto-generated method stub
	return null;
}

@Override
public OrgResult queryUserOrg(LoginUser loginUser, String accountCode) {
	// TODO Auto-generated method stub
	return null;
}

@Override
public DeptResult queryUserDept(LoginUser loginUser, String accountCode) {
	// TODO Auto-generated method stub
	return null;
}



@Override
public CommonResult queryResState(LoginUser loginUser, String resCode) {
	// TODO Auto-generated method stub
	return null;
}

@Override
public ResourceResult queryResDetail(LoginUser loginUser, String resCode,
		String resName) {
	// TODO Auto-generated method stub
	return null;
}

@Override
public CommonResult resIsBelongOrg(LoginUser loginUser, String resCode) {
	// TODO Auto-generated method stub
	return null;
}

@Override
public CommonResult resOwnRelaton(LoginUser loginUser, String resCode1,
		String resCode2) {
	// TODO Auto-generated method stub
	return null;
}

@Override
public OrgResult queryResOrg(LoginUser loginUser, String resCode) {
	// TODO Auto-generated method stub
	return null;
}

@Override
public AppResult queryResApp(LoginUser loginUser, String resCode) {
	// TODO Auto-generated method stub
	return null;
}

@Override
public RoleResult queryResRole(LoginUser loginUser, String resCode) {
	// TODO Auto-generated method stub
	return null;
}


@Override
public DeptResult queryDeptDetail(LoginUser loginUser, String deptName) {
	// TODO Auto-generated method stub
	return null;
}

@Override
public DeptResult queryUpDept(LoginUser loginUser, String deptId) {
	// TODO Auto-generated method stub
	return null;
}

@Override
public DeptResult queryNextDept(LoginUser loginUser, String deptId) {
	// TODO Auto-generated method stub
	return null;
}


@Override
public CommonResult queryDeptState(LoginUser loginUser, String deptId) {
	// TODO Auto-generated method stub
	return null;
}

@Override
public OrgResult queryOrgDetail(LoginUser loginUser) {
	// TODO Auto-generated method stub
	return null;
}

@Override
public AppResult queryOrgApps(LoginUser loginUsere) {
	// TODO Auto-generated method stub
	return null;
}

@Override
public ResourceResult queryOrgResources(LoginUser loginUser) {
	// TODO Auto-generated method stub
	return null;
}

@Override
public DeptResult queryOrgDepts(LoginUser loginUser) {
	// TODO Auto-generated method stub
	return null;
}



@Override
public CommonResult queryOrgState(LoginUser loginUser) {
	// TODO Auto-generated method stub
	return null;
}

@Override
public AppResult queryAppDetail(LoginUser loginUser) {
	// TODO Auto-generated method stub
	return null;
}

@Override
public OrgResult queryAppOrg(LoginUser loginUser) {
	// TODO Auto-generated method stub
	return null;
}

@Override
public ResourceResult queryAppRes(LoginUser loginUser) {
	// TODO Auto-generated method stub
	return null;
}

@Override
public RoleResult queryAppRoles(LoginUser loginUser) {
	// TODO Auto-generated method stub
	return null;
}

@Override
public CommonResult queryAppState(LoginUser loginUser) {
	// TODO Auto-generated method stub
	return null;
}

@Override
public LoginResult logout(String orgId, String account) {
	// TODO Auto-generated method stub
	return null;
}

@Override
public boolean doGet(String accountCode, String Message)
		throws ServletException, IOException {
	// TODO Auto-generated method stub
	return false;
}

@Override
public boolean doGroupGet(List<String> accountCodes, String Message)
		throws ServletException, IOException {
	// TODO Auto-generated method stub
	return false;
}

@Override
public AccountResultbak queryAccountDetail(LoginUser loginUser,
		String accountCode) {
	// TODO Auto-generated method stub
	return null;
}

@Override
public AccountResultbak queryResAccount(LoginUser loginUser, String resCode) {
	// TODO Auto-generated method stub
	return null;
}

@Override
public AccountResultbak queryDeptAccount(LoginUser loginUser, String deptId) {
	// TODO Auto-generated method stub
	return null;
}

@Override
public AccountResultbak queryOrgAccount(LoginUser loginUser) {
	// TODO Auto-generated method stub
	return null;
}
}
