package com.hession.service;

import java.util.List;

import com.hession.domain.User;

public interface HelloWorld {
	
	public void setGreeting(String greeting);

	public String hello();

	public User getUser();
	
	public List<User> getUsers();
}
