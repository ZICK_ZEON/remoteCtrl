package com.hession.back;

import java.io.Serializable;

public class RegisterResult implements Serializable {

	 
	private static final long serialVersionUID = 1L;

	private Long id;
	private String code;
	private String message;
	 
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	
}
