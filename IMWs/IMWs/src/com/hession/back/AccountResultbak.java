package com.hession.back;

import java.io.Serializable;
import java.util.List;

import com.hession.domain.User;

public class AccountResultbak implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int code;
	private String msg;
	private List<User> accUsers;
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public List<User> getAccUsers() {
		return accUsers;
	}
	public void setAccUsers(List<User> accUsers) {
		this.accUsers = accUsers;
	}
	

}
