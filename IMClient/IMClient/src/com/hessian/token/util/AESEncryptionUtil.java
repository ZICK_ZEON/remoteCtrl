package com.hessian.token.util;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;




/**
 * @author zhangwei
 * 
 */
public class AESEncryptionUtil {
	/**
	 * 加密
	 * @param bytes
	 * @param aesKey
	 * @return
	 */
	public static byte[] encryptMessage(byte[] bytes, String aesKey) {
		try {
			Object obj = KeyUtil.getKey(aesKey);
			SecretKey key = (SecretKey) obj;
			Cipher cipher = Cipher.getInstance("AES");
			cipher.init(Cipher.ENCRYPT_MODE, key);
			byte[] decryptedBytes = cipher.doFinal(bytes);
			return decryptedBytes;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * 解密
	 * @param encryptedBytes
	 * @param aesKey
	 * @return
	 */
	public static byte[] decryptMessage(byte[] encryptedBytes, String aesKey) {
		try {
			Object obj = KeyUtil.getKey(aesKey);
			SecretKey key = (SecretKey) obj;
			Cipher cipher = Cipher.getInstance("AES");
			cipher.init(Cipher.DECRYPT_MODE, key);
			byte[] bytes = cipher.doFinal(encryptedBytes);
			return bytes;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
