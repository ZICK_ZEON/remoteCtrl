package com.hessian.token;

import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.security.Security;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;

import com.sun.crypto.provider.SunJCE;

/**
 * @author zhangwei
 *
 */
public class AESKeyCreater {
	
	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception{
		String url = System.getProperty("user.dir");
		AESKeyCreater aESKeyCreater = new AESKeyCreater();
		aESKeyCreater.createKeyFile(url);
	}

	public void createKeyFile(String url) throws Exception{
		Security.addProvider(new SunJCE());
		KeyGenerator keygen = KeyGenerator.getInstance("AES");
		SecretKey key = keygen.generateKey();
		File file = new File(url + "/src/com/hessian/resources/AES_Secret_Key_File.aes");
		FileOutputStream out = new FileOutputStream(file);
		ObjectOutputStream objectOutput = new ObjectOutputStream(out);
		objectOutput.writeObject(key);
		objectOutput.flush();
		objectOutput.close();
	}
}

