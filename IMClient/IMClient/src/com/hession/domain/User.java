package com.hession.domain;

import java.io.Serializable;

public class User implements Serializable {
	
	private static final long serialVersionUID = 8870835581336118202L;
	

	private String accountName;
	private String accountCode;
	public String getAccountName() {
		return accountName;
	}
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}
	public String getAccountCode() {
		return accountCode;
	}
	public void setAccountCode(String accountCode) {
		this.accountCode = accountCode;
	}
	
}
