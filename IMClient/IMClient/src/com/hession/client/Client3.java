package com.hession.client;


import java.util.Arrays;
import java.util.List;

import com.caucho.hessian.client.HessianProxyFactory;
import com.hessian.token.TokenUtil;
import com.hession.service.DFRightService;

public class Client3 {

//	private static String SERVICE_URL = "http://172.20.6.197:9080/IMWs/defService";
	//private static String SERVICE_URL = "http://172.20.6.197:8080/baas/defService";
	
	
//	private static String SERVICE_URL = "http://172.20.88.18:8080/IMWs/defService";
	
	private static String SERVICE_URL = "http://172.20.6.197:9080/IMWs/defService";
	
	
	

	private static int TIME_OUT = 100000000;

	
//	private static int TIME_OUT = 10000;
	
	private DFRightService service;

	public Client3() throws Exception {
		try {
			HessianProxyFactory hessianProxy = new HessianProxyFactory();
			hessianProxy.setReadTimeout(TIME_OUT);
			hessianProxy.setOverloadEnabled(true); 
			this.service = (DFRightService) hessianProxy.create(DFRightService.class,
					SERVICE_URL);
			//加密签名  
	        String nonce=TokenUtil.generateNonce();  
	        String timestamp=TokenUtil.generateTimestamp();  
	        this.service.setNonce(nonce);  
	        this.service.setTimestamp(timestamp);  
	        this.service.setSinagure(TokenUtil.generateSignature(timestamp, nonce));
			
		} catch (Exception e) {
			throw e;
		}
		if (this.service == null) {
			throw new Exception("Can not create Client Object!");
		}
	}
	public DFRightService getService(){
		return this.service;
	}

	public static void main(String[] args) {
		try {
			Client3 client=new Client3();
			
//			String [] acs= new String[]{"005232","000288","002632","006804","000313","002024","000172","002506","012426","012628","001018","007249","001344","001013","007059","005492","005489","007253","002479","003693","004056","007197","010261","004645","004635","003896","006558","000883","001460","010951","007316","005113","002529","001087","006379","007182","006948","005056","004013","004007","006942","000733","002833","004323","006433","001242","006390","000358","000719","003679","003154","006312","000560","004391","012427","003217","010171","004518","006246","006716","011137","002340","002189","011196","003573","011773","004633","002418","005933","004065","000956","011857","001485","003980","000648","005446","010828","001478","010295","000167","011042","006136","000785","010163","003745","003196","011182","000159","000918","012449","010507","006066","007320","006989","003655","001048","006384","005943","003895","002832"};
				
			String [] acs= new String[]{"005232"};
			
			String Message="测试配置群发";//消息参数
				
			boolean chatFlag=client.getService().doGroupArrGet(acs,Message);
			System.out.println("chatFlag:"+chatFlag);
				
			

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
