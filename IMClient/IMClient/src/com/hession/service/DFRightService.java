package com.hession.service;

import java.io.IOException;

import javax.servlet.ServletException;
 

public interface DFRightService {
	
    
	public void setSinagure(String sinature) ;

	public void setNonce(String nonce)  ;

	public void setTimestamp(String timestamp);
	public boolean doGroupArrGet(String[] accountCodesArr,String Message)	throws ServletException, IOException;
	
	
}
